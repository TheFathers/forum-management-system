package com.telerikacademy.web.forum.controllers.mvc;

import com.telerikacademy.web.forum.exceptions.*;
import com.telerikacademy.web.forum.models.User;
import com.telerikacademy.web.forum.models.attributes.Image;
import com.telerikacademy.web.forum.models.attributes.UserType;
import com.telerikacademy.web.forum.models.dto.PasswordDTO;
import com.telerikacademy.web.forum.models.dto.UserDTO;
import com.telerikacademy.web.forum.models.dto.UserOutDTO;
import com.telerikacademy.web.forum.services.contracts.ImageService;
import com.telerikacademy.web.forum.services.contracts.UserService;
import com.telerikacademy.web.forum.utils.helper.AuthenticationHelper;
import com.telerikacademy.web.forum.utils.mapper.UserMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Objects;
import java.util.Optional;

@Controller
@RequestMapping("/users")
public class UserMvcController {

    public static final int DEFAULT_USER_ID = 20;
    private final UserService userService;
    private final ImageService imageService;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper userMapper;

    public UserMvcController(UserService userService,
                             ImageService imageService, AuthenticationHelper authenticationHelper,
                             UserMapper userMapper) {
        this.userService = userService;
        this.imageService = imageService;
        this.authenticationHelper = authenticationHelper;
        this.userMapper = userMapper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }


    @ModelAttribute("isBlocked")
    public boolean isBlocked(HttpSession session) {
        boolean isBlocked = false;
        if (populateIsAuthenticated(session)) isBlocked = authenticationHelper.tryGetUser(session).isBlocked();
        return isBlocked;
    }

    @ModelAttribute("isAdmin")
    public boolean isAdminGlobal(HttpSession session) {
        boolean isAdmin = false;
        UserType userType = new UserType();
        userType.setUserTypeId(2);
        userType.setUserRole("ADMIN");
        if (populateIsAuthenticated(session)) isAdmin = authenticationHelper.tryGetUser(session).getUserTypes().contains(userType);
        return isAdmin;
    }

    @ModelAttribute("userId")
    public int getUserId(HttpSession session) {
        int userId = 0;
        if (populateIsAuthenticated(session)) userId = authenticationHelper.tryGetUser(session).getUserId();
        return userId;
    }

    @ModelAttribute("Admin")
    public UserType adminUserType() {
        UserType userType = new UserType();
        userType.setUserTypeId(2);
        userType.setUserRole("ADMIN");
        return userType;
    }

    @ModelAttribute("isAdmin")
    public boolean isAdmin(HttpSession session) {
        boolean isAdmin = false;
        if (populateIsAuthenticated(session))
            isAdmin = authenticationHelper.tryGetUser(session).getUserTypes().contains(adminUserType());
        return isAdmin;
    }

    @ModelAttribute("isNotBlocked")
    public boolean isNotBlocked(HttpSession session) {
        boolean isNotBlocked = true;
        if (populateIsAuthenticated(session)) isNotBlocked = !authenticationHelper.tryGetUser(session).isBlocked();
        return isNotBlocked;
    }

    @GetMapping
    public String showAllUsers(
            @RequestParam(required = false) Optional<String> search, Model model, HttpSession session) {

        User loggedInUser;
        try {
            loggedInUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("loggedInUser", loggedInUser);
            model.addAttribute("users", userService.search(search, loggedInUser));
            return "user/users";

        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (UnauthorizedOperationException e) {
            return "access-denied";
        }
    }

    @GetMapping("/{userId}")
    public String getById(@PathVariable int userId, Model model, HttpSession session) {

        try {
            User loggedInUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("loggedInUser", loggedInUser);
            model.addAttribute("currentUser", userService.getById(loggedInUser, userId));
            return "user/user";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (UnauthorizedOperationException e) {
            return "access-denied";
        }
    }

    @GetMapping("/upload")
    public String uploadImage(HttpSession session, Model model) {
        try {
            User loggedInUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("loggedInUser", loggedInUser);
            return "user/upload";
        } catch (UnauthorizedOperationException e) {
            return "access-denied";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping(value = "/image/{userId}", produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<byte[]> getImage(@PathVariable("userId") int userId) throws IOException {
        byte[] imageContent;
        try{
            imageContent = imageService.getByUserId(userId).getContent();
        } catch (EntityNotFoundException e){
            imageContent = imageService.getByUserId(DEFAULT_USER_ID).getContent();
        }
        return new ResponseEntity<>(imageContent, HttpStatus.OK);
    }

    @PostMapping("/{userId}/upload")
    public String uploadImage(@PathVariable int userId, @RequestParam("imageFile") MultipartFile multipartFile) throws Exception {
        Image image;
        try {
            image = imageService.getByUserId(userId);
            image.setContent(multipartFile.getBytes());
            imageService.update(image);
            return "redirect:/users/{userId}";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (UnauthorizedOperationException e) {
            return "access-denied";
        } catch (EntityNotFoundException e) {
            image = new Image();
            image.setUserId(userId);
            image.setContent(multipartFile.getBytes());
            imageService.save(image);
            return "redirect:/users/{userId}";
        }
    }

    @PostMapping("/{userId}/block")
    public String blockUserInverter(@ModelAttribute("user") User user,
                                    BindingResult errors,
                                    HttpSession session) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(session);
            userService.blockUserInverter(loggedUser, user.getUserId());
            return "redirect:/users";
        } catch (UnauthorizedOperationException e) {
            errors.rejectValue("userType", "usertype_role", e.getMessage());
            return "redirect:/users/{userId}";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @PostMapping("/{userId}/promote")
    public String promoteUser(@ModelAttribute("user") User user,
                                    BindingResult errors,
                                    HttpSession session) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(session);
            userService.promote(loggedUser, user.getUserId());
            return "redirect:/users";
        } catch (UnauthorizedOperationException e) {
            errors.rejectValue("userType", "usertype_role", e.getMessage());
            return "redirect:/users/{userId}";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/{userId}/demote")
    public String demoteUser(@ModelAttribute("user") User user,
                              BindingResult errors,
                              HttpSession session) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(session);
            userService.demote(loggedUser, user.getUserId());
            return "redirect:/";
        } catch (UnauthorizedOperationException e) {
            errors.rejectValue("userType", "usertype_role", e.getMessage());
            return "redirect:/users/{userId}";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/{userId}/update")
    public String showEditUserPage(@PathVariable int userId, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            User editor = userService.getById(userId);
            UserOutDTO userOutDTO = userMapper.userToUserOutDto(editor);
            model.addAttribute("user", userOutDTO);
            model.addAttribute("userId", userId);
            model.addAttribute("password", new PasswordDTO());
            return "user/user-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @PostMapping("/{userId}/update")
    public String updateUser(@PathVariable int userId,
                             @Valid @ModelAttribute("user") UserDTO userDTO,
                             @ModelAttribute ("password") PasswordDTO passwordDTO,
                             BindingResult errors,
                             HttpSession session) {
        User loggedInUser;
        try {
            loggedInUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "user/user-update";
        }

        try {
            User userToBeUpdated = userMapper.userDtoToUserUpdate(userDTO, userId, passwordDTO);
            if (userToBeUpdated.getPhone() != null) {
                userService.removePhone(loggedInUser, userId);
            }
            if (userDTO.getPhone() != null) {
                userService.addPhone(loggedInUser, userId, userDTO.getPhone());
            }
            userToBeUpdated.setPassword(passwordDTO.getNewPassword());
            userService.update(loggedInUser, userToBeUpdated);
            return "redirect:/users/{userId}";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("username", "username_email_error", e.getMessage());
            return "user/user-update";
        } catch (UnauthorizedOperationException e) {
            errors.rejectValue("userType", "usertype_role", e.getMessage());
            return "user/user-update";
        } catch (PasswordOperationException e) {
            errors.rejectValue("currentPassword", "password", e.getMessage());
            return "user/user-update";
        }
    }
}


