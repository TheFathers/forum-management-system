package com.telerikacademy.web.forum.controllers.mvc;

import com.telerikacademy.web.forum.models.attributes.UserType;
import com.telerikacademy.web.forum.services.contracts.PostService;
import com.telerikacademy.web.forum.services.contracts.UserService;
import com.telerikacademy.web.forum.utils.helper.AuthenticationHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    private final PostService postService;
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;

    public HomeMvcController(PostService postService, UserService userService, AuthenticationHelper authenticationHelper) {
        this.postService = postService;
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isBlocked")
    public boolean isBlocked(HttpSession session) {
        boolean isBlocked = false;
        if (populateIsAuthenticated(session)) isBlocked = authenticationHelper.tryGetUser(session).isBlocked();
        return isBlocked;
    }

    @ModelAttribute("userId")
    public int getUserId(HttpSession session) {
        int userId = 0;
        if (populateIsAuthenticated(session)) userId = authenticationHelper.tryGetUser(session).getUserId();
        return userId;
    }

    @ModelAttribute("isAdmin")
    public boolean isAdmin(HttpSession session) {
        boolean isAdmin = false;
        UserType userType = new UserType();
        userType.setUserTypeId(2);
        userType.setUserRole("ADMIN");
        if (populateIsAuthenticated(session)) isAdmin = authenticationHelper.tryGetUser(session).getUserTypes().contains(userType);
        return isAdmin;
    }

    @GetMapping
    public String showHomePage(Model model){

            model.addAttribute("createdposts", postService.getPostsCount());
            model.addAttribute("activeusers", userService.getActiveUsersCount());
            model.addAttribute("mostrecent", postService.getMostRecent());
            model.addAttribute("mostcommented", postService.getMostCommented());
        return "index";
    }
}
