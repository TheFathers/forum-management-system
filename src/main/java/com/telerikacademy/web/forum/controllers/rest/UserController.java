package com.telerikacademy.web.forum.controllers.rest;

import com.telerikacademy.web.forum.exceptions.DuplicateEntityException;
import com.telerikacademy.web.forum.exceptions.EntityNotFoundException;
import com.telerikacademy.web.forum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.forum.models.User;
import com.telerikacademy.web.forum.models.dto.*;
import com.telerikacademy.web.forum.services.contracts.UserService;
import com.telerikacademy.web.forum.utils.helper.AuthenticationHelper;
import com.telerikacademy.web.forum.utils.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UserController {
    private final UserService service;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper mapper;


    @Autowired
    public UserController(UserService service,
                          AuthenticationHelper authenticationHelper,
                          UserMapper userMapper) {
        this.service = service;
        this.authenticationHelper = authenticationHelper;
        this.mapper = userMapper;
    }

    @GetMapping
    public List<UserOutDTO> getAll(@RequestHeader HttpHeaders headers,
                             @RequestParam(required = false) Optional<String> search){
        try {
            User user = authenticationHelper.tryGetUser(headers);
            List<User> users = service.search(search, user);
            return mapper.userToOutDtos(users);

        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED,
                    e.getMessage());
        } catch (EntityNotFoundException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage());
        }
    }

    @GetMapping("/{userId}")
    public UserOutDTO getUser(@RequestHeader HttpHeaders headers, @PathVariable int userId){
        try {
            User loggedInUser = authenticationHelper.tryGetUser(headers);
            User user = service.getById(loggedInUser, userId);
            return mapper.userToOutDto(user);

        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED,
                    e.getMessage());
        } catch (EntityNotFoundException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage());
        }
    }

    @PostMapping
    public UserOutDTO create(@Valid @RequestBody RegisterDTO registerDto) {
        try {
            User user = mapper.registerDtoToUserCreate(registerDto);
            service.create(user);
            return mapper.userToOutDto(user);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT,
                    e.getMessage());
        }catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage());
        }

    }

    // TODO - add block request - BlockUser() - Update user by id
    @PutMapping("/block/{userId}")
    public void blockUserInverter(@RequestHeader HttpHeaders headers, @PathVariable int userId) {
        try {
            User loggedInUser = authenticationHelper.tryGetUser(headers);
            service.blockUserInverter(loggedInUser, userId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage()
            );
        } catch (UnauthorizedOperationException e){
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED,
                    e.getMessage()
            );
        }
    }

    @PutMapping("/{userId}")
    public UserOutDTO update(@RequestHeader HttpHeaders headers,@PathVariable int userId,
                             @Valid @RequestBody UserDTO userDTO, @ModelAttribute ("password") PasswordDTO passwordDTO) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            User updatedUser = mapper.userDtoToUserUpdate(userDTO, userId, passwordDTO);

            service.update(user, updatedUser);
            return mapper.userToOutDto(user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage()
            );
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT,
                    e.getMessage()
            );
        } catch (UnauthorizedOperationException e){
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED,
                    e.getMessage()
            );
        }
    }

    @PutMapping("/promote/{userId}")
    public String promote(@RequestHeader HttpHeaders headers,@PathVariable int userId) {
        try {
            User loggedInUser = authenticationHelper.tryGetUser(headers);
            service.promote(loggedInUser, userId);
            return String.format("User with id %d was successfully promoted.", userId);
        } catch (UnauthorizedOperationException e){
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED,
                    e.getMessage()
            );
        }
    }

    @PutMapping("/demote/{userId}")
    public String demote(@RequestHeader HttpHeaders headers,@PathVariable int userId) {
        try {
            User loggedInUser = authenticationHelper.tryGetUser(headers);
            service.demote(loggedInUser, userId);
            return String.format("User with id %d was successfully demoted.", userId);
        }catch (DuplicateEntityException e) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT,
                    e.getMessage()
            );
        } catch (UnauthorizedOperationException e){
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED,
                    e.getMessage()
            );
        }
    }

    @PutMapping("/phone/{userId}")
    public String addPhone(@RequestHeader HttpHeaders headers, @PathVariable int userId,
                           @RequestBody @Valid PhoneDTO phoneDTO) {
        try {
            User loggedInUser = authenticationHelper.tryGetUser(headers);
            service.addPhone(loggedInUser, userId, phoneDTO.getPhone());
            return String.format("Phone number %s was added to user with id %d.", phoneDTO.getPhone(), userId);
        } catch (UnauthorizedOperationException e){
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED,
                    e.getMessage()
            );
        }
    }

    @DeleteMapping("/phone/{userId}")
    public String removePhone(@RequestHeader HttpHeaders headers, @PathVariable int userId) {
        try {
            User loggedInUser = authenticationHelper.tryGetUser(headers);
            service.removePhone(loggedInUser, userId);
            return String.format("Phone number was removed from user with id %d.", userId);
        } catch (UnauthorizedOperationException e){
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED,
                    e.getMessage()
            );
        }
    }

}
