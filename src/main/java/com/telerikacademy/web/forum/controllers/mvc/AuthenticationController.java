package com.telerikacademy.web.forum.controllers.mvc;

import com.telerikacademy.web.forum.exceptions.AuthenticationFailureException;
import com.telerikacademy.web.forum.exceptions.DuplicateEntityException;
import com.telerikacademy.web.forum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.forum.models.User;
import com.telerikacademy.web.forum.models.attributes.UserType;
import com.telerikacademy.web.forum.models.dto.LoginDTO;
import com.telerikacademy.web.forum.models.dto.RegisterDTO;
import com.telerikacademy.web.forum.services.contracts.UserService;
import com.telerikacademy.web.forum.utils.helper.AuthenticationHelper;
import com.telerikacademy.web.forum.utils.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/auth")
public class AuthenticationController {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper userMapper;

    @Autowired
    public AuthenticationController(UserService userService,
                                    AuthenticationHelper authenticationHelper,
                                    UserMapper userMapper) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.userMapper = userMapper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }


    @ModelAttribute("isAdmin")
    public boolean isAdmin(HttpSession session) {
        boolean isAdmin = false;
        UserType userType = new UserType();
        userType.setUserTypeId(2);
        userType.setUserRole("ADMIN");
        if (populateIsAuthenticated(session)) isAdmin = authenticationHelper.tryGetUser(session).getUserTypes().contains(userType);
        return isAdmin;
    }

    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("login", new LoginDTO());
        return "login";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("login") LoginDTO login,
                              BindingResult bindingResult,
                              HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "login";
        }

        try {
            authenticationHelper.verifyAuthentication(login.getUsername(), login.getPassword());
            session.setAttribute("currentUser", login.getUsername());
            return "redirect:/";
        } catch (AuthenticationFailureException e) {
            bindingResult.rejectValue("username", "auth_error", e.getMessage());
            return "login";
        }
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUser");
        return "redirect:/auth/login";
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("register", new RegisterDTO());
        return "register";
    }

    @PostMapping("/register")
    public String handleRegister(@Valid @ModelAttribute("register") RegisterDTO register,
                                 BindingResult bindingResult ) {
        if (bindingResult.hasErrors()) {
            return "register";
        }

        if (!register.getPassword().equals(register.getPasswordConfirm())) {
            bindingResult.rejectValue("passwordConfirm", "password_error", "Password confirmation should match password.");
            return  "register";
        }

        try {
            User user = userMapper.registerDtoToUserCreate(register);
            userService.create(user);
            return "redirect:/auth/login";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("username", "username_email_error", e.getMessage());
            return "register";
        } catch (UnauthorizedOperationException e) {
            bindingResult.rejectValue("userType", "usertype_role", e.getMessage());
            return "register";
        }
    }

}
