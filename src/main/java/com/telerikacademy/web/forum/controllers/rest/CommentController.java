package com.telerikacademy.web.forum.controllers.rest;

import com.telerikacademy.web.forum.exceptions.EntityNotFoundException;
import com.telerikacademy.web.forum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.forum.models.Comment;
import com.telerikacademy.web.forum.models.User;
import com.telerikacademy.web.forum.models.dto.CommentDTO;
import com.telerikacademy.web.forum.services.contracts.CommentService;
import com.telerikacademy.web.forum.utils.helper.AuthenticationHelper;
import com.telerikacademy.web.forum.utils.mapper.CommentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/comments")
public class CommentController {

    private final CommentService service;
    private final CommentMapper commentMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CommentController(CommentService service, CommentMapper commentMapper, AuthenticationHelper authenticationHelper) {
        this.service = service;
        this.commentMapper = commentMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/{postId}")
    public List<Comment> getByPostId(@PathVariable int postId) {
        try {
            return service.getByPostId(postId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("{postId}")
    public Comment create(@RequestHeader HttpHeaders headers, @PathVariable int postId,
                          @Valid @RequestBody CommentDTO commentDTO) {
        try {
            User editor = authenticationHelper.tryGetUser(headers);
            Comment comment = commentMapper.commentDtoToComment(commentDTO, editor, postId);
            service.create(comment, editor);
            return comment;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{postId}/{commentId}")
    public Comment update(@RequestHeader HttpHeaders headers, @PathVariable int postId,
                          @PathVariable int commentId, @Valid @RequestBody CommentDTO commentDTO) {
        try {
            User editor = authenticationHelper.tryGetUser(headers);
            Comment comment = commentMapper.commentDtoToCommentUpdate(commentDTO, commentId, editor, postId);
            service.update(comment, editor);
            return comment;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }

    }

    @DeleteMapping("/{commentId}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int commentId) {
        try {
            User editor = authenticationHelper.tryGetUser(headers);
            service.delete(commentId, editor);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

}
