package com.telerikacademy.web.forum.controllers.rest;

import com.telerikacademy.web.forum.exceptions.EntityNotFoundException;
import com.telerikacademy.web.forum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.forum.models.Post;
import com.telerikacademy.web.forum.models.User;
import com.telerikacademy.web.forum.models.attributes.PostVote;
import com.telerikacademy.web.forum.models.dto.PostDTO;
import com.telerikacademy.web.forum.models.dto.PostOutDTO;
import com.telerikacademy.web.forum.models.dto.TagDTO;
import com.telerikacademy.web.forum.services.contracts.PostService;
import com.telerikacademy.web.forum.utils.helper.AuthenticationHelper;
import com.telerikacademy.web.forum.utils.mapper.PostMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/posts")
public class PostController {

    private final PostService service;
    private final PostMapper postMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public PostController(PostService service, PostMapper postMapper,
                          AuthenticationHelper authenticationHelper) {
        this.service = service;
        this.postMapper = postMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/mostrecent")
    public List<PostOutDTO> getMostRecent() {
        return postMapper.postsToOutDTOs(service.getMostRecent());
    }

    @GetMapping("/mostcommented")
    public List<PostOutDTO> getMostCommented() {
        return postMapper.postsToOutDTOs(service.getMostCommented());
    }

    @GetMapping()
    public List<PostOutDTO> search(@RequestHeader HttpHeaders headers,
                                   @RequestParam(required = false) Optional<String> search){
        try{
            User user = authenticationHelper.tryGetUser(headers);
            return postMapper.postsToOutDTOs(service.search(search, user));
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED,
                    e.getMessage());
        } catch (EntityNotFoundException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage());
        }
    }

    @GetMapping("/filter")
    public List<PostOutDTO> getAll(@RequestHeader HttpHeaders headers,
                                   @RequestParam(required = false) Optional<String> keyword,
                                   @RequestParam(required = false) Optional<String> sortBy) {
        try {
            authenticationHelper.tryGetUser(headers);
            return postMapper.postsToOutDTOs(service.filter(keyword, sortBy));
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED,
                    e.getMessage());
        } catch (EntityNotFoundException e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage());
        }
    }

    @PostMapping
    public PostOutDTO create(@RequestHeader HttpHeaders headers, @Valid @RequestBody PostDTO postDTO) {
        try {
            User editor = authenticationHelper.tryGetUser(headers);
            Post post = postMapper.postDtoToPostCreate(postDTO, editor);
            service.create(editor, post);
            return postMapper.postToOutDTO(post);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,e.getMessage());
        }
    }

    @PutMapping("/{postId}/tags")
    public void addTags(@RequestHeader HttpHeaders headers, @PathVariable int postId,
                             @Valid @RequestBody TagDTO tagDTO) {
        try {
            User editor = authenticationHelper.tryGetUser(headers);
            List<String> tagsToBeAdded = tagDTO.getTagNames();
            if (!tagsToBeAdded.isEmpty())
                service.addTags(editor, postId, tagsToBeAdded);

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,e.getMessage());
        }
    }

    @PutMapping("/{postId}/{tagId}")
    public void updateTag(@RequestHeader HttpHeaders headers, @PathVariable int postId,
                           @PathVariable int tagId, @RequestBody String tagName) {
        try {
            User editor = authenticationHelper.tryGetUser(headers);
            service.userTagUpdate(tagName, tagId, postId, editor);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,e.getMessage());
        }
    }

    @PutMapping("/admin/{tagId}")
    public void updateTag(@RequestHeader HttpHeaders headers, @PathVariable int tagId, @RequestBody String tagName) {
        try {
            User editor = authenticationHelper.tryGetUser(headers);
            service.adminTagUpdate(tagName, tagId, editor);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,e.getMessage());
        }
    }

    @DeleteMapping("/{postId}/{tagId}")
    public void deleteTag(@RequestHeader HttpHeaders headers, @PathVariable int postId,
                          @PathVariable int tagId) {
        try {
            User editor = authenticationHelper.tryGetUser(headers);
            service.userTagDelete(tagId, postId, editor);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,e.getMessage());
        }
    }

    @DeleteMapping("/admin/{tagId}")
    public void deleteTag(@RequestHeader HttpHeaders headers, @PathVariable int tagId) {
        try {
            User editor = authenticationHelper.tryGetUser(headers);
            service.adminTagDelete(tagId, editor);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,e.getMessage());
        }
    }


    @PutMapping("/{postId}")
    public Post update(@RequestHeader HttpHeaders headers, @PathVariable int postId,
                       @Valid @RequestBody PostDTO postDTO) {
        try {
            User editor = authenticationHelper.tryGetUser(headers);
            Post post = postMapper.postDtoToPostUpdate(postDTO, postId);
            service.update(postId, post, editor);
            return post;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{postId}/vote/{voteType}")
    public void upVote(@RequestHeader HttpHeaders headers, @PathVariable int postId, @PathVariable String voteType) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            PostVote postVote = postMapper.VoteTypeToVote(postId, voteType, user.getUserId());
            service.upVote(postId, user, postVote);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{postId}/vote")
    public void voteDelete(@RequestHeader HttpHeaders headers, @PathVariable int postId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            service.deleteVote(postId, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


    @DeleteMapping("/{postId}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int postId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            service.delete(postId, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
