package com.telerikacademy.web.forum.controllers.mvc;

import com.telerikacademy.web.forum.exceptions.AuthenticationFailureException;
import com.telerikacademy.web.forum.exceptions.DuplicateEntityException;
import com.telerikacademy.web.forum.exceptions.EntityNotFoundException;
import com.telerikacademy.web.forum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.forum.models.Comment;
import com.telerikacademy.web.forum.models.Post;
import com.telerikacademy.web.forum.models.User;
import com.telerikacademy.web.forum.models.attributes.PostVote;
import com.telerikacademy.web.forum.models.attributes.Tag;
import com.telerikacademy.web.forum.models.attributes.UserType;
import com.telerikacademy.web.forum.models.dto.*;
import com.telerikacademy.web.forum.models.enums.PostSortOptions;
import com.telerikacademy.web.forum.models.enums.VoteType;
import com.telerikacademy.web.forum.services.contracts.*;
import com.telerikacademy.web.forum.utils.helper.AuthenticationHelper;
import com.telerikacademy.web.forum.utils.mapper.CommentMapper;
import com.telerikacademy.web.forum.utils.mapper.PostMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/posts")
public class PostMvcController {

    private final PostService postService;
    private final CommentService commentService;
    private final UserService userService;
    private final TagService tagService;
    private final PostVoteService postVoteService;
    private final PostMapper postMapper;
    private final CommentMapper commentMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public PostMvcController(PostService postService, CommentService commentService, UserService userService, TagService tagService, PostVoteService postVoteService, PostMapper postMapper, CommentMapper commentMapper, AuthenticationHelper authenticationHelper) {

        this.postService = postService;
        this.commentService = commentService;
        this.userService = userService;
        this.tagService = tagService;
        this.postVoteService = postVoteService;
        this.postMapper = postMapper;
        this.commentMapper = commentMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isBlocked")
    public boolean isBlocked(HttpSession session) {
        boolean isBlocked = false;
        if (populateIsAuthenticated(session)) isBlocked = authenticationHelper.tryGetUser(session).isBlocked();
        return isBlocked;
    }

    @ModelAttribute("userId")
    public int getUserId(HttpSession session) {
        int userId = 0;
        if (populateIsAuthenticated(session)) userId = authenticationHelper.tryGetUser(session).getUserId();
        return userId;
    }

    @ModelAttribute("isAdmin")
    public boolean isAdmin(HttpSession session) {
        boolean isAdmin = false;
        UserType userType = new UserType();
        userType.setUserTypeId(2);
        userType.setUserRole("ADMIN");
        if (populateIsAuthenticated(session)) isAdmin = authenticationHelper.tryGetUser(session).getUserTypes().contains(userType);
        return isAdmin;
    }

    @ModelAttribute("tags")
    public List<Tag> populateTags() {
        return tagService.getAll();
    }

    @ModelAttribute("sortPostOptions")
    public PostSortOptions[] populateSortOptions() {
        return PostSortOptions.values();
    }

    @GetMapping
    public String showAllPosts(@RequestParam(required = false) Optional<String> search,
                               Model model,
                               HttpSession httpSession) {
        try {
            User loggedInUser = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("loggedInUser", loggedInUser);
            model.addAttribute("filterPostDto", new FilterPostDto());
            model.addAttribute("posts", postService.search(search, loggedInUser));
            return "post/posts";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (UnauthorizedOperationException e) {
            return "access-denied";
        }
    }

    @PostMapping("/filter")
    public String filterBeers(@ModelAttribute("filterPostDto") FilterPostDto filterPostDto,
                              Model model, HttpSession httpSession) {

        try {
            User loggedInUser = authenticationHelper.tryGetUser(httpSession);
            model.addAttribute("loggedInUser", loggedInUser);
            var filtered = postService.mvcFilter(
                    Optional.ofNullable(filterPostDto.getTagId() == null ? null : filterPostDto.getTagId()),
                    Optional.ofNullable(filterPostDto.getSort() == null ? null : PostSortOptions.valueOfPreview(filterPostDto.getSort()))
            );
            model.addAttribute("posts", filtered);
            return "post/posts";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (UnauthorizedOperationException e) {
            return "access-denied";
        }
    }

    @GetMapping("/{postId}")
    public String showSinglePost(@PathVariable int postId, Model model, HttpSession httpSession) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            Post post = postService.getById(postId);
            List<PostVote> postVotes = postVoteService.getAll();
            model.addAttribute("post", post);
            model.addAttribute("newComment", new CommentDTO());
            model.addAttribute("isNotLiked", isNotLiked(postId, user, postVotes));
            model.addAttribute("isNotDisliked", isNotDisliked(postId, user, postVotes));
            return "post/post";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{postId}/like")
    public String like(@PathVariable int postId,
                       Model model,
                       HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            PostVote like = postMapper.VoteTypeToVote(postId, "like", user.getUserId());
            postService.upVote(postId, user, like);
            return "redirect:/posts/{postId}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @PostMapping("/{postId}/dislike")
    public String dislike(@PathVariable int postId,
                          Model model,
                          HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            PostVote dislike = postMapper.VoteTypeToVote(postId, "dislike", user.getUserId());
            postService.upVote(postId, user, dislike);
            return "redirect:/posts/{postId}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/new")
    public String showNewPostPage(Model model, HttpSession httpSession){
        User author;
        try {
            author = authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("post", new PostDTO());
        model.addAttribute("author", author);
        return "post/post-new";
    }

    @PostMapping("/new")
    public String createPost(@Valid @ModelAttribute("post") PostDTO postDTO,
                             BindingResult errors,
                             Model model, HttpSession httpSession) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "post/post-new";
        }

        try {
            Post post = postMapper.postDtoToPostCreate(postDTO, user);
            postService.create(user, post);
            return "redirect:/posts";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }


    @PostMapping("/{postId}/comment")
    public String createComment(@PathVariable int postId,
                             @Valid @ModelAttribute("newComment") CommentDTO commentDTO,
                             BindingResult errors,
                             HttpSession session) {
        User loggedInUser;
        try {
            loggedInUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "post/post";
        }

        try {
            Comment comment = commentMapper.commentDtoToComment(commentDTO, loggedInUser, postId);
            commentService.create(comment, loggedInUser);
            return "redirect:/posts/{postId}#newcomment";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (UnauthorizedOperationException e) {
            errors.rejectValue("userType", "usertype_role", e.getMessage());
            return "user/user-update";
        }
    }

    @GetMapping("/{postId}/update")
    public String showEditPostPage(@PathVariable int postId, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            Post post = postService.getById(postId);
            PostOutDTO postOutDTO = postMapper.postToOutDTO(post);
            model.addAttribute("postId", postId);
            model.addAttribute("post", postOutDTO);
            return "post/post-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{postId}/update")
    public String updatePost(@PathVariable int postId,
                             @Valid @ModelAttribute("post") PostDTO postDTO,
                             BindingResult errors,
                             Model model,
                             HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "post/post-update";
        }

        try {
            Post post = postMapper.postDtoToPostUpdate(postDTO, postId);
            postService.update(postId, post, user);
            return "redirect:/posts/{postId}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/{postId}/{commentId}/update")
    public String showEditCommentPage(@PathVariable int postId,
                                      @PathVariable int commentId,
                                      Model model,
                                      HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            Comment comment = commentService.getById(commentId);
            CommentOutDTO commentOutDTO = commentMapper.commentToCommentDto(comment);
            model.addAttribute("comment", commentOutDTO);
            model.addAttribute("postId", postId);
            return "post/comment-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{postId}/{commentId}/update")
    public String updateComment(@PathVariable int postId,
                                @PathVariable int commentId,
                             @Valid @ModelAttribute("comment") CommentDTO commentDTO,
                             BindingResult errors,
                             Model model,
                             HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "post/comment-update";
        }

        try {
            Comment comment = commentMapper.commentDtoToCommentUpdate(commentDTO, commentId);
            commentService.update(comment, user);
            return "redirect:/posts/{postId}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/{postId}/{commentId}/delete")
    public String deleteComment(@PathVariable int postId,
                                @PathVariable int commentId, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            commentService.delete(commentId, user);
            return "redirect:/posts/{postId}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/{postId}/delete")
    public String deletePost(@PathVariable int postId, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            postService.delete(postId, user);

            return "redirect:/posts";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    private boolean isNotLiked(int postId, User user, List<PostVote> postVotes) {
        boolean isNotDisliked = true;
        Optional<PostVote> postVote = postVoteService.getByUserAndPost(postId, user.getUserId());
        if (postVote.isPresent() && postVote.get().getVote().getVoteType().toString().equalsIgnoreCase("like")){
            isNotDisliked = false;
        }
        return isNotDisliked;
    }

    private boolean isNotDisliked(int postId, User user, List<PostVote> postVotes) {
        boolean isNotDisliked = true;
        Optional<PostVote> postVote = postVoteService.getByUserAndPost(postId, user.getUserId());
        if (postVote.isPresent() && postVote.get().getVote().getVoteType().toString().equalsIgnoreCase("dislike")){
            isNotDisliked = false;
        }
        return isNotDisliked;
    }

}
