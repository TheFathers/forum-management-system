package com.telerikacademy.web.forum.exceptions;

public class PasswordOperationException extends RuntimeException {
    public PasswordOperationException(String message) {
        super(message);
    }
}
