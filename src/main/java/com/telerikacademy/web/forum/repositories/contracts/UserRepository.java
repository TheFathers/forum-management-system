package com.telerikacademy.web.forum.repositories.contracts;

import com.telerikacademy.web.forum.models.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {

    List<User> getAll();

    long getActiveUsersCount();

    List<User> search(Optional<String> search);

    User getById(int userId);

    User getByEmail(String email);

    User getByUsername(String username);

    void create(User user);

    void blockUserInverter(int userId);

    void update(User user);
}
