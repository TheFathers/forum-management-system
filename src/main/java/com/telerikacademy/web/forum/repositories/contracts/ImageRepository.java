package com.telerikacademy.web.forum.repositories.contracts;

import com.telerikacademy.web.forum.models.attributes.Image;

public interface ImageRepository {

    Image getByUserId(int userId);

    void save(Image image);

    void update(Image image);
}
