package com.telerikacademy.web.forum.repositories.contracts;

import com.telerikacademy.web.forum.models.attributes.PostVote;
import com.telerikacademy.web.forum.models.attributes.Vote;
import com.telerikacademy.web.forum.models.enums.VoteType;

import java.util.List;
import java.util.Optional;

public interface PostVoteRepository {

    List<PostVote> getAll();

    Vote getByVoteType(VoteType voteType);

    List<PostVote> getByUserId(int userId);

    List<PostVote> getByPostId(int postId);

    Optional<PostVote> getByUserAndPost(int postId, int userId);

    void create(PostVote postVote);

    void delete(PostVote postVote);

    void update(PostVote postVote);
}
