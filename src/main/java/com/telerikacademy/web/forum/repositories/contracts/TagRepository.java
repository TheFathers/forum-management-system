package com.telerikacademy.web.forum.repositories.contracts;

import com.telerikacademy.web.forum.models.attributes.Tag;

import java.util.List;
import java.util.Optional;

public interface TagRepository {

    List<Tag> getAll();

    void create(Tag tag);

    void update(Tag tag);

    void adminTagDelete(int tagId);

    void userTagDelete(int tagId, int postId);

    Tag getById(int tagId);

    Tag getByTagName(String tagName);

    Optional<Tag> getByName(String tag);
}
