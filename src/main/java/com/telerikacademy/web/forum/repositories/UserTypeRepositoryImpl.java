package com.telerikacademy.web.forum.repositories;

import com.telerikacademy.web.forum.exceptions.EntityNotFoundException;
import com.telerikacademy.web.forum.models.attributes.UserType;
import com.telerikacademy.web.forum.repositories.contracts.UserTypeRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserTypeRepositoryImpl implements UserTypeRepository {

    SessionFactory sessionFactory;

    @Autowired
    public UserTypeRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public UserType getUserType(String userRole) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserType> query = session.createQuery(
                    "from UserType where userRole = :userRole", UserType.class);
            query.setParameter("userRole", userRole);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("UserType", "user role", userRole.toString());
            }
            return query.list().get(0);
        }
    }
}
