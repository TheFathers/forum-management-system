package com.telerikacademy.web.forum.repositories.contracts;

import com.telerikacademy.web.forum.models.Post;
import com.telerikacademy.web.forum.models.enums.PostSortOptions;

import java.util.List;
import java.util.Optional;

public interface PostRepository {

    long getPostCount();

    List<Post> getAll();

    List<Post> getMostRecent();

    List<Post> getMostCommented();

    List<Post> search(Optional<String> search);

    List<Post> filter(Optional<String> keyword, Optional<String> sortBy);

    List<Post> mvcFilter(Optional<Integer> styleId, Optional<PostSortOptions> sortOptions);

    Post getById(int postId);

    List<Integer> getUserPostsIds(int userId);

    void create(Post post);

    void update(Post post);

    void delete(Post post);
}
