package com.telerikacademy.web.forum.repositories.contracts;

import com.telerikacademy.web.forum.models.attributes.UserType;

public interface UserTypeRepository {

    UserType getUserType(String userRole);
}
