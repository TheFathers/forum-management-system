package com.telerikacademy.web.forum.repositories;

import com.telerikacademy.web.forum.exceptions.EntityNotFoundException;
import com.telerikacademy.web.forum.models.User;
import com.telerikacademy.web.forum.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from Users", User.class);
            return query.list();
        }
    }

    @Override
    public long getActiveUsersCount() {
        try(Session session = sessionFactory.openSession()){
            return (long) session.createQuery(
                    "select count(*) from Users where isBlocked = false").uniqueResult();
        }
    }

    @Override
    public List<User> search(Optional<String> search) {
        if (search.isEmpty()){
            return getAll();
        }
        try (Session session = sessionFactory.openSession()){
            Query<User> query = session.createQuery
                    ("from Users where username like :name " +
                            "or firstName like :name " +
                            "or lastName like :name " +
                            "or email like :name", User.class);
            query.setParameter("name", "%" + search.get() + "%");
            return query.list();
        }
    }

    @Override
    public User getById(int userId) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, userId);
            if (user == null) {
                throw new EntityNotFoundException("User", userId);
            }
            return user;
        }
    }

    @Override
    public User getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from Users where email = :email", User.class);
            query.setParameter("email", email);
            return query.getSingleResult();
        } catch (NoResultException e) {
            throw new EntityNotFoundException("User", "email", email);
        }
    }

    @Override
    public User getByUsername(String username) {

        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from Users where username = :username ", User.class);
            query.setParameter("username", username);
            if (query.list().isEmpty()){
                throw new EntityNotFoundException("User", "username", username);
            }
            return query.list().get(0);
        }
    }

    @Override
    public void create(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void blockUserInverter(int userId) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            User user = session.get(User.class, userId);
            user.setBlocked(!user.isBlocked()); //switching to opposite
            session.update(user);
            session.getTransaction().commit();
        } catch (NoResultException e) {
            throw new EntityNotFoundException("User", userId);
        }
    }

    @Override
    public void update(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }
}
