package com.telerikacademy.web.forum.repositories;

import com.telerikacademy.web.forum.exceptions.EntityNotFoundException;
import com.telerikacademy.web.forum.models.attributes.Image;
import com.telerikacademy.web.forum.repositories.contracts.ImageRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class ImageRepositoryImpl implements ImageRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ImageRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Image getByUserId(int userId) {
        try (Session session = sessionFactory.openSession()){
            return session.createQuery("from Images where userId = :userId", Image.class)
                    .setParameter("userId", userId)
                    .getSingleResult();
        } catch (NoResultException e){
            throw new EntityNotFoundException("Image", "user id", String.valueOf(userId));
        }
    }

    @Override
    public void save(Image image) {
        try (Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.save(image);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Image image) {
        try (Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(image);
            session.getTransaction().commit();
        }
    }
}
