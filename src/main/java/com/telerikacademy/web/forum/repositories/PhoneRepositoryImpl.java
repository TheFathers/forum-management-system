package com.telerikacademy.web.forum.repositories;

import com.telerikacademy.web.forum.models.attributes.Phone;
import com.telerikacademy.web.forum.repositories.contracts.PhoneRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.Optional;

@Repository
public class PhoneRepositoryImpl implements PhoneRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public PhoneRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Optional<Phone> getByUserId(int userId) {
        Optional<Phone> phone = Optional.empty();
        try(Session session = sessionFactory.openSession()){
            Query<Phone> query = session.createQuery
                    ("from Phones where userId = :userId", Phone.class);
            query.setParameter("userId", userId);
            phone = Optional.ofNullable(query.getSingleResult());
            return phone;
        } catch (NoResultException e){
            return phone;
        }
    }

    @Override
    public void create(Phone phoneToBe) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.save(phoneToBe);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Phone phoneToBe) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(phoneToBe);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(Phone phone) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.delete(phone);
            session.getTransaction().commit();
        }
    }
}
