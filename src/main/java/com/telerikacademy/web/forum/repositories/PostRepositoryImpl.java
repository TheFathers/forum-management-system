package com.telerikacademy.web.forum.repositories;

import com.telerikacademy.web.forum.exceptions.EntityNotFoundException;
import com.telerikacademy.web.forum.models.Post;
import com.telerikacademy.web.forum.models.enums.PostSortOptions;
import com.telerikacademy.web.forum.repositories.contracts.PostRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Repository
public class PostRepositoryImpl implements PostRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public PostRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public long getPostCount() {
        try(Session session = sessionFactory.openSession()){
            return (long) session.createQuery("select count(*) from Posts").uniqueResult();
        }
    }

    @Override
    public List<Post> getAll() {
        try(Session session = sessionFactory.openSession()){
            return session.createQuery("from Posts order by postId desc", Post.class).list();
        }
    }

    @Override
    public List<Post> getMostRecent() {
        try(Session session = sessionFactory.openSession()){
            return session.createQuery
                            ("from Posts order by creationTime desc", Post.class)
                    .setMaxResults(10)
                    .list();
        }
    }

    @Override
    public List<Post> getMostCommented() {
        try(Session session = sessionFactory.openSession()){
            return session.createQuery
                    ("Select p from Posts p left join p.comments  " +
                            "group by p.postId order by COUNT(*) desc", Post.class)
                    .setMaxResults(10)
                    .list();
        }
    }

    @Override
    public List<Post> search(Optional<String> search) {
        if(search.isEmpty()){
            return getAll();
        }
        try(Session session = sessionFactory.openSession()){
            return session.createQuery("select p from Posts p " +
                    "left join p.tags t " +
                    "where t.tagName like :keyword " +
                    "or p.title like :keyword " +
                    "or p.content like :keyword", Post.class)
                    .setParameter("keyword", "%" + search.get() + "%")
                    .list();
        }
    }

    @Override
    public List<Post> filter(Optional<String> keyword, Optional<String> sortBy) {

        try(Session session = sessionFactory.openSession()){
            var queryString = new StringBuilder(" from Posts ");
            var filter = new ArrayList<String>();
            var queryParams = new HashMap<String, Object>();

            keyword.ifPresent(value -> {
                filter.add(" title like :keyword ");
                queryParams.put("keyword", "%" + value + "%");
            });


            if (!filter.isEmpty()) {
                queryString.append(" where ").append(String.join(" and ", filter));
            }

            sortBy.ifPresent(value -> queryString.append(generateSortString(value)));

            Query<Post> queryList = session.createQuery(queryString.toString(), Post.class);
            queryList.setProperties(queryParams);

            System.out.println(queryString);
            return queryList.list();
        }
    }

    @Override
    public List<Post> mvcFilter(Optional<Integer> tagId, Optional<PostSortOptions> sortOptions) {
        try (Session session = sessionFactory.openSession()) {
            var baseQuery = new StringBuilder("select p from Posts p left join p.votes v ");
            var filters = new ArrayList<String>();
            var params = new HashMap<String, Object>();

            tagId.ifPresent(id -> {
                filters.add(" t.tagId = :tagId");
                params.put("tagId", id);
            });

            if (!filters.isEmpty()) {
                baseQuery.append(" left join p.tags t where ").append(String.join(" and ", filters));
            }

            if (sortOptions.isPresent()){
                PostSortOptions currentSort = sortOptions.get();
                switch (currentSort){
                    case CREATION_ASC:
                    case CREATION_DESC:
                        baseQuery.append(currentSort.getQuery());
                        break;
                    case MOST_LIKES:
                    case MOST_DISLIKES:
                        if (tagId.isPresent()) baseQuery.append(" and ").append(currentSort.getQuery());
                        if (tagId.isEmpty()) baseQuery.append(" where ").append(currentSort.getQuery());
                        break;

                }
            }

            return session.createQuery(baseQuery.toString(), Post.class)
                    .setProperties(params)
                    .list();
        }
    }

    private String generateSortString(String value) {
        var queryString = new StringBuilder(" order by ");
        String[] params = value.split("_");

        if (value.isEmpty()) {
            // return " order by name ";
            throw new UnsupportedOperationException("Sort should have maximum two params divided by _ symbol");
        }

        switch (params[0]) {
            case "creationTime":
                queryString.append(" creationTime ");
                break;
            case "postId":
                queryString.append(" postId ");
                break;
        }

        if (params.length > 1 && params[1].equals("desc")) {
            queryString.append(" desc ");
        }
        if (params.length > 2) {
            throw new UnsupportedOperationException("Sort should have maximum two params divided by _ symbol");
        }

        return queryString.toString();
    }

    public Post getById(int postId) {
        try(Session session = sessionFactory.openSession()){
            Query<Post> query = session.createQuery("from Posts where postId = :postId", Post.class);
            query.setParameter("postId", postId);
            return query.getSingleResult();
        } catch (NoResultException e){
            throw new EntityNotFoundException("Post", postId);
        }
    }

    @Override
    public List<Integer> getUserPostsIds(int userId) {
        try(Session session = sessionFactory.openSession()){
            Query<Post> query = session.createQuery("from Posts where user.userId = :userId", Post.class)
                    .setParameter("userId", userId);
            List<Integer> postIds = new ArrayList<>();
            query.list().forEach(post -> postIds.add(post.getPostId()));
            return postIds;
        }
    }

    @Override
    public void create(Post post) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.save(post);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Post post) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(post);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(Post post) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.delete(post);
            session.getTransaction().commit();
        }
    }
}
