package com.telerikacademy.web.forum.repositories.contracts;

import com.telerikacademy.web.forum.models.attributes.Phone;

import java.util.Optional;

public interface PhoneRepository {

    Optional<Phone> getByUserId(int userId);

    void create(Phone phoneToBe);

    void update(Phone phoneToBe);

    void delete(Phone phone);
}
