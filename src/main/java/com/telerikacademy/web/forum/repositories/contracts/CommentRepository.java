package com.telerikacademy.web.forum.repositories.contracts;

import com.telerikacademy.web.forum.models.Comment;

import java.util.List;

public interface CommentRepository {

    List<Comment> getByPostId(int postId);

    Comment getById(int commentId);

    int getOwnerId(int commentId);

    void create(Comment comment);

    void update(Comment newComment);

    void delete(Comment comment);
}
