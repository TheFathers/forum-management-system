package com.telerikacademy.web.forum.repositories;

import com.telerikacademy.web.forum.exceptions.EntityNotFoundException;
import com.telerikacademy.web.forum.models.attributes.Tag;
import com.telerikacademy.web.forum.repositories.contracts.TagRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;
import java.util.Optional;

@Repository
public class TagRepositoryImpl implements TagRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public TagRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Tag> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Tags", Tag.class).list();
        }
    }

    @Override
    public void create(Tag tag) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.save(tag);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Tag tag) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(tag);
            session.getTransaction().commit();
        }
    }

    @Override
    public void adminTagDelete(int tagId) {
        try(Session session = sessionFactory.openSession()){
            Transaction transaction = session.beginTransaction();
            session.createNativeQuery("delete from post_tags where tag_id = :tagId")
                    .setParameter("tagId", tagId).executeUpdate();
            session.createNativeQuery("delete from tags where tag_id = :tagId")
                    .setParameter("tagId", tagId).executeUpdate();
            transaction.commit();
        }
    }

    @Override
    public void userTagDelete(int tagId, int postId) {


        try(Session session = sessionFactory.openSession()){
            Transaction transaction = session.beginTransaction();
            session.createNativeQuery("delete from post_tags where tag_id = :tagId and post_id = :postId")
                    .setParameter("tagId", tagId).setParameter("postId", postId).executeUpdate();
            transaction.commit();
        }
    }

    @Override
    public Tag getById(int tagId) {
        try(Session session = sessionFactory.openSession()){
            Query<Tag> query = session.createQuery
                    ("from Tags where tagId = :tagId", Tag.class);
            query.setParameter("tagId", tagId);
            return query.getSingleResult();
        } catch (NoResultException e){
            throw new EntityNotFoundException("Tag", tagId);
        }
    }

    @Override
    public Tag getByTagName(String tagName) {
        try(Session session = sessionFactory.openSession()){
            Query<Tag> query = session.createQuery
                    ("from Tags where tagName = :tagName", Tag.class);
            query.setParameter("tagName", tagName);
            return query.getSingleResult();
        } catch (NoResultException e){
            throw new EntityNotFoundException("Tag", "tag name", tagName);
        }
    }

    @Override
    public Optional<Tag> getByName(String tagName) {
        Optional<Tag> tag = Optional.empty();
        try(Session session = sessionFactory.openSession()){
            Query<Tag> query = session.createQuery
                    ("from Tags where tagName = :tagName", Tag.class);
            query.setParameter("tagName", tagName);
            tag = Optional.ofNullable(query.getSingleResult());
            return tag;
        } catch (NoResultException e){
            return tag;
        }
    }
}
