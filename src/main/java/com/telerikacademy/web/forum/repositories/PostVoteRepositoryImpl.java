package com.telerikacademy.web.forum.repositories;

import com.telerikacademy.web.forum.exceptions.EntityNotFoundException;
import com.telerikacademy.web.forum.models.attributes.PostVote;
import com.telerikacademy.web.forum.models.attributes.Vote;
import com.telerikacademy.web.forum.models.enums.VoteType;
import com.telerikacademy.web.forum.repositories.contracts.PostVoteRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;
import java.util.Optional;

@Repository
public class PostVoteRepositoryImpl implements PostVoteRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public PostVoteRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<PostVote> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("From PostsVotes", PostVote.class).list();
        }
    }

    @Override
    public Vote getByVoteType(VoteType voteType) {
        try (Session session = sessionFactory.openSession()){
            return session.createQuery("From Votes where voteType = :voteType ", Vote.class)
                    .setParameter("voteType", voteType)
                    .getSingleResult();
        } catch (NoResultException e){
            throw new EntityNotFoundException("Vote", "vote type", voteType.toString());
        }
    }

    @Override
    public Optional<PostVote> getByUserAndPost(int postId, int userId) {
        Optional<PostVote> postVote = Optional.empty();
        try (Session session = sessionFactory.openSession()){
            Query<PostVote> query = session.createQuery(
                    "From PostsVotes where postId = :postId and userId = :userId ", PostVote.class)
                    .setParameter("postId", postId)
                    .setParameter("userId", userId);
            postVote = Optional.ofNullable(query.getSingleResult());
            return postVote;
        } catch (NoResultException e){
            return postVote;
        }
    }

    @Override
    public List<PostVote> getByUserId(int userId) {
        try (Session session = sessionFactory.openSession()){
            return session.createQuery("From PostsVotes where userId = :userId ", PostVote.class)
                    .setParameter("userId", userId)
                    .list();
        }
    }

    @Override
    public List<PostVote> getByPostId(int postId) {
        try (Session session = sessionFactory.openSession()){
            return session.createQuery(
                    "From PostsVotes where postId = :postId ", PostVote.class)
                    .setParameter("postId", postId)
                    .list();
        }
    }

    @Override
    public void update(PostVote postVote) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(postVote);
            session.getTransaction().commit();
        }
    }

    @Override
    public void create(PostVote postVote) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.save(postVote);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(PostVote postVote) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.delete(postVote);
            session.getTransaction().commit();
        }
    }


}
