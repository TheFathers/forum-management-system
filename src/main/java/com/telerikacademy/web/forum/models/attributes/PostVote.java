package com.telerikacademy.web.forum.models.attributes;

import javax.persistence.*;
import java.util.Objects;

@Entity(name = "PostsVotes")
@Table(name = "posts_votes")
public class PostVote {

    @Id
    @Column(name = "post_vote_id")
    private int postVoteId;

    @Column(name = "post_Id")
    private int postId;

    @ManyToOne
    @JoinColumn(name = "vote_id")
    private Vote vote;

    @Column(name = "user_Id")
    private int userId;

    public PostVote() {
    }

    public int getPostVoteId() {
        return postVoteId;
    }

    public void setPostVoteId(int postVoteId) {
        this.postVoteId = postVoteId;
    }

    public Vote getVote() {
        return vote;
    }

    public void setVote(Vote vote) {
        this.vote = vote;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PostVote postVote = (PostVote) o;
        return postId == postVote.postId && userId == postVote.userId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(postId, userId);
    }
}
