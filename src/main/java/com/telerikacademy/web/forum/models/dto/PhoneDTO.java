package com.telerikacademy.web.forum.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class PhoneDTO {

    @NotNull(message = "Phone cannot be null")
    @Positive(message = "Phone should be positive")
    private String phone;

    public PhoneDTO() {
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
