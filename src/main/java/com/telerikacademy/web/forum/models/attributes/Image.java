package com.telerikacademy.web.forum.models.attributes;

import javax.persistence.*;

@Entity(name = "Images")
@Table(name = "images")
public class Image {

    @Id
    @Column(name = "user_id")
    int userId;

    @Lob
    @Column(name = "content")
    byte[] content;

//        String name;


    public Image() {
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }
}
