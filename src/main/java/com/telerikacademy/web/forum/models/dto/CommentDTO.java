package com.telerikacademy.web.forum.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class CommentDTO {

    @NotNull(message = "Content should be valid")
    @Size(min = 4, max = 8192, message = "Comment should contain between 4 and 8192 symbols")
    private String content;

    public CommentDTO() {
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
