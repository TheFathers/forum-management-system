package com.telerikacademy.web.forum.models.dto;

import com.telerikacademy.web.forum.models.attributes.Image;

import javax.validation.constraints.*;

public class UserDTO {

    private Image image;

    private String username;

    @NotNull (message = "First name cannot be null.")
    private String firstName;

    @NotEmpty(message = "Last name can't be empty")
    private String lastName;

//    @NotNull(message = "Password should be valid")
    private String password;

    @NotNull(message = "Email should be valid")
    private String email;

    private String phone;

    public UserDTO() {
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}