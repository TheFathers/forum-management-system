package com.telerikacademy.web.forum.models.dto;

public class FilterPostDto {

    private Integer tagId;

    private String sort;

    public FilterPostDto() {
    }

    public Integer getTagId() {
        return tagId;
    }

    public void setTagId(Integer tagId) {
        this.tagId = tagId;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }
}
