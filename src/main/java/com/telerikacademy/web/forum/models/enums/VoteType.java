package com.telerikacademy.web.forum.models.enums;

public enum VoteType {

    LIKE, DISLIKE;

    @Override
    public String toString() {
        switch(this){
            case LIKE:
                return "Like";
            case DISLIKE:
                return "Dislike";
            default:
                throw new IllegalArgumentException();
        }
    }
}
