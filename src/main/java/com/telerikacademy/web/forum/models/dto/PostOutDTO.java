package com.telerikacademy.web.forum.models.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import java.util.List;

public class PostOutDTO {

    private int postId;

    private String title;

    private String content;

    private String creationDate;

    private int userId;

    @JsonInclude(Include.NON_NULL)
    private List<String> tagNames;

    @JsonInclude(Include.NON_NULL)
    private List<String> postVotes;

    public PostOutDTO() {
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public List<String> getTagNames() {
        return tagNames;
    }

    public void setTagNames(List<String> tagNames) {
        this.tagNames = tagNames;
    }

    public List<String> getPostVotes() {
        return postVotes;
    }

    public void setPostVotes(List<String> postVotes) {
        this.postVotes = postVotes;
    }
}
