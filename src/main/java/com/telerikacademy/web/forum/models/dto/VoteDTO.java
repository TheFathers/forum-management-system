package com.telerikacademy.web.forum.models.dto;

public class VoteDTO {

    private String voteType;

    public VoteDTO() {
    }

    public String getVoteType() {
        return voteType;
    }

    public void setVoteType(String voteType) {
        this.voteType = voteType;
    }
}
