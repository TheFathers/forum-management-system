package com.telerikacademy.web.forum.models.attributes;

import javax.persistence.*;
import java.util.Objects;

@Entity(name = "Phones")
@Table(name = "phones")
public class Phone {

    @Id
    @Column(name = "user_id")
    public int userId;

    @Column(name = "phone")
    public String phone;

    public Phone() {
    }

    public Phone(int userId, String phone) {
        this.userId = userId;
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getUser() {
        return userId;
    }

    public void setUser(int userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Phone phone1 = (Phone) o;
        return userId == phone1.userId && phone.equals(phone1.phone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, phone);
    }
}
