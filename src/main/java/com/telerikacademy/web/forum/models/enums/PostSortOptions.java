package com.telerikacademy.web.forum.models.enums;

import java.util.HashMap;
import java.util.Map;

public enum PostSortOptions {

    CREATION_ASC("Oldest", " group by p.postId order by p.creationTime "),
    CREATION_DESC("Most Recent", " group by p.postId order by p.creationTime desc "),
    MOST_LIKES("Most liked", " v.voteId = 1 group by p.postId order by count(p.postId) desc "),
    MOST_DISLIKES("Most disliked", " v.voteId = 2 group by p.postId order by count(p.postId) desc ");

    private final String preview;
    private final String query;

    private static final Map<String, PostSortOptions> BY_PREVIEW = new HashMap<>();

    static {
        for (PostSortOptions option : values()) {
            BY_PREVIEW.put(option.getPreview(), option);
        }
    }

    PostSortOptions(String preview, String query) {
        this.preview = preview;
        this.query = query;
    }

    public static PostSortOptions valueOfPreview(String preview) {
        return BY_PREVIEW.get(preview);
    }

    public String getPreview() {
        return preview;
    }

    public String getQuery() {
        return query;
    }
}
