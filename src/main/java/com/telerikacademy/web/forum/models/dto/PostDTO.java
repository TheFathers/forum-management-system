package com.telerikacademy.web.forum.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class PostDTO {

    @NotNull(message = "Post title cannot be null.")
    @Size(min = 4, max = 32, message = "Title should be between 4 and 32 symbols")
    private String title;

    @NotNull(message = "Post content cannot be null.")
    @Size(min = 4, max = 8192, message = "Content should be between 4 and 8192 symbols")
    private String content;

    private List<Integer> tagIds;

    private List<String> tagNames;

    public PostDTO(){
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<Integer> getTagIds() {
        return tagIds;
    }

    public void setTagIds(List<Integer> tagIds) {
        this.tagIds = tagIds;
    }

    public List<String> getTagNames() {
        return tagNames;
    }

    public void setTagNames(List<String> tagNames) {
        this.tagNames = tagNames;
    }
}
