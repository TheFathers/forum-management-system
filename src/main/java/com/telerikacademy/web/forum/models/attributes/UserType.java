package com.telerikacademy.web.forum.models.attributes;

import javax.persistence.*;
import java.util.Objects;

@Entity(name = "UserType")
@Table(name = "types")
public class UserType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "type_id")
    private int userTypeId;

    @Column(name = "user_role")
    private String userRole;

    public UserType() {
    }

    public int getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(int userTypeId) {
        this.userTypeId = userTypeId;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserType userType = (UserType) o;
        return userRole.equals(userType.userRole);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userRole);
    }
}
