package com.telerikacademy.web.forum.models.attributes;

import com.telerikacademy.web.forum.models.enums.VoteType;

import javax.persistence.*;

@Entity(name = "Votes")
@Table(name = "votes")
public class Vote {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "vote_id")
    private int voteId;

    @Column(name = "vote_type")
    @Enumerated(EnumType.STRING)
    private VoteType voteType;

    public Vote() {
    }

    public Vote(VoteType voteType) {
        this.voteType = voteType;
    }

    public int getVoteId() {
        return voteId;
    }

    public void setVoteId(int voteId) {
        this.voteId = voteId;
    }

    public VoteType getVoteType() {
        return voteType;
    }

    public void setVoteType(VoteType voteType) {
        this.voteType = voteType;
    }
}
