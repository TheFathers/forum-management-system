package com.telerikacademy.web.forum.utils.mapper;

import com.telerikacademy.web.forum.exceptions.PasswordOperationException;
import com.telerikacademy.web.forum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.forum.models.Post;
import com.telerikacademy.web.forum.models.User;
import com.telerikacademy.web.forum.models.attributes.Phone;
import com.telerikacademy.web.forum.models.attributes.UserType;
import com.telerikacademy.web.forum.models.dto.PasswordDTO;
import com.telerikacademy.web.forum.models.dto.RegisterDTO;
import com.telerikacademy.web.forum.models.dto.UserDTO;
import com.telerikacademy.web.forum.models.dto.UserOutDTO;
import com.telerikacademy.web.forum.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class UserMapper {

    private final UserRepository userRepository;

    @Autowired
    public UserMapper(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User registerDtoToUserCreate(RegisterDTO registerDto){
        User user = dtoToUserCreate(registerDto);
        return user;
    }

    public UserOutDTO userToUserOutDto(User user){
        UserOutDTO userOutDTO = new UserOutDTO();
        if (user.getImage() != null) userOutDTO.setImage(user.getImage());
        userOutDTO.setImage(user.getImage());
        userOutDTO.setUsername(user.getUsername());
        userOutDTO.setFirstName(user.getFirstName());
        userOutDTO.setLastName(user.getLastName());
        userOutDTO.setEmail(user.getEmail());
        if (user.getPhone() != null) userOutDTO.setPhone(user.getPhone().phone);
        userOutDTO.setPassword(user.getPassword());

        List<String> userTypes = new ArrayList<>();
        user.getUserTypes().forEach(userType -> userTypes.add(userType.getUserRole()));
        userOutDTO.setUserTypes(userTypes);
        return userOutDTO;
    }

    public User userDtoToUserUpdate(UserDTO userDTO, int userId, PasswordDTO passwordDTO){
        User user = userRepository.getById(userId);
        dtoToUserUpdate(userDTO, user, passwordDTO);
        return user;
    }


    public UserOutDTO userToOutDto(User user) {
        return userToOutDTO(user);
    }

    public List<UserOutDTO> userToOutDtos(List<User> users) {
        List<UserOutDTO> result = new ArrayList<>();
        usersToOutDtos(result, users);
        return result;
    }

    public void usersToOutDtos(List<UserOutDTO> result, List<User> users){
        for (User user : users) {
            UserOutDTO userOutDTO = userToOutDTO(user);
            result.add(userOutDTO);
        }
    }

    private UserOutDTO userToOutDTO(User user) {
        UserOutDTO userOutDTO = new UserOutDTO();
        Set<Post> userPosts = user.getPosts();
        List<String> postTitles = userOutDTO.getPostTitles();

        userOutDTO.setUserId(user.getUserId());
        userOutDTO.setUsername(user.getUsername());
        userOutDTO.setFirstName(user.getFirstName());
        userOutDTO.setLastName(user.getLastName());
        userOutDTO.setEmail(user.getEmail());
        if (!userPosts.isEmpty())userPosts.forEach(post -> postTitles.add(post.getTitle()));
        if (user.getPhone() != null) userOutDTO.setPhone(user.getPhone().getPhone());
        user.getUserTypes().forEach(userType -> userOutDTO.getUserTypes().add(userType.toString()));

        return userOutDTO;
    }

    public User dtoToUserCreate(RegisterDTO registerDto) {
        User user = new User();
        user.setUsername(registerDto.getUsername());
        user.setPassword(registerDto.getPassword());
        user.setFirstName(registerDto.getFirstName());
        user.setLastName(registerDto.getLastName());
        user.setEmail(registerDto.getEmail());

        UserType userType = new UserType();
        userType.setUserTypeId(1);
        userType.setUserRole("USER");
        Set<UserType> userTypes = new HashSet<>();
        userTypes.add(userType);
        user.setUserTypes(userTypes);

        return user;
    }

    public void dtoToUserUpdate(UserDTO userDTO, User user, PasswordDTO passwordDTO){
        if (userDTO.getImage() != null) user.setImage(userDTO.getImage());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setEmail(userDTO.getEmail());

        if (!Objects.equals(passwordDTO.getCurrentPassword(), user.getPassword())) {
            throw new PasswordOperationException("Please enter correct password.");
        }

        if (!Objects.equals(passwordDTO.getNewPassword(), passwordDTO.getConfirmPassword())) {
            throw new PasswordOperationException("Passwords do not match.");
        }
    }
}
