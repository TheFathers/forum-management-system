package com.telerikacademy.web.forum.utils.mapper;

import com.telerikacademy.web.forum.models.Post;
import com.telerikacademy.web.forum.models.User;
import com.telerikacademy.web.forum.models.attributes.PostVote;
import com.telerikacademy.web.forum.models.attributes.Tag;
import com.telerikacademy.web.forum.models.attributes.Vote;
import com.telerikacademy.web.forum.models.dto.PostDTO;
import com.telerikacademy.web.forum.models.dto.PostOutDTO;
import com.telerikacademy.web.forum.models.enums.VoteType;
import com.telerikacademy.web.forum.repositories.contracts.PostRepository;
import com.telerikacademy.web.forum.repositories.contracts.TagRepository;
import com.telerikacademy.web.forum.repositories.contracts.PostVoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;
import java.util.*;

@Component
public class PostMapper {

    private final PostRepository postRepository;
    private final TagRepository tagRepository;
    private final PostVoteRepository voteRepository;

    @Autowired
    public PostMapper(PostRepository postRepository, TagRepository tagRepository, PostVoteRepository voteRepository) {
        this.postRepository = postRepository;
        this.tagRepository = tagRepository;
        this.voteRepository = voteRepository;
    }


    public Post postDtoToPostCreate(PostDTO postDTO, User author){
        Post post = new Post();
        postDtoToPostCreate(postDTO, post, author);
        return post;
    }
    public Post postDtoToPostUpdate(PostDTO postDTO, int postId){
        Post post = postRepository.getById(postId);
        PostDtoToPostUpdate(postDTO, post);
        return post;
    }

    public List<PostOutDTO> postsToOutDTOs(List<Post> posts) {
        List<PostOutDTO> result = new ArrayList<>();
        postsToOutDTO(result, posts);
        return result;
    }

    public void postDtoToPostCreate(PostDTO postDTO, Post post, User author){
        post.setTitle(postDTO.getTitle());
        post.setContent(postDTO.getContent());
        post.setUser(author);

        addTagsToPost(postDTO, post);
    }

    public void postsToOutDTO(List<PostOutDTO> result, List<Post> posts){
        for (Post post : posts) {
            PostOutDTO postOutDTO = postToOutDTO(post);
            result.add(postOutDTO);
        }
    }

    public PostOutDTO postToOutDTO(Post post) {
        PostOutDTO postOutDTO = new PostOutDTO();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");

        postOutDTO.setPostId(post.getPostId());
        postOutDTO.setTitle(post.getTitle());
        postOutDTO.setContent(post.getContent());
        postOutDTO.setUserId(post.getUser().getUserId());
        postOutDTO.setCreationDate(formatter.format(post.getCreationTime()));
        if (post.getTags() != null && !post.getTags().isEmpty()){
            List<String> tagList = new ArrayList<>();
            post.getTags().forEach(tag -> tagList.add(tag.getTagName()));
            postOutDTO.setTagNames(tagList);
        }
        if (post.getVotes() != null && !post.getVotes().isEmpty()){
            List<String> postVotes = new ArrayList<>();
            post.getVotes().forEach(vote -> postVotes.add(vote.getVoteType().toString()));
            postOutDTO.setPostVotes(postVotes);
        }

        return postOutDTO;
    }

    public void PostDtoToPostUpdate(PostDTO postDTO, Post post){
        post.setTitle(postDTO.getTitle());
        post.setContent(postDTO.getContent());

        addTagsToPost(postDTO, post);
    }

    private void addTagsToPost(PostDTO postDTO, Post post) {
        Set<Tag> tags = new HashSet<>();
        if (postDTO.getTagNames() != null && !postDTO.getTagNames().isEmpty()){
            for (String tagName: postDTO.getTagNames()) {
                Tag currentTag = tagRepository.getByTagName(tagName);
                tags.add(currentTag);
            }
        }

        if (postDTO.getTagIds() != null && !postDTO.getTagIds().isEmpty()){
            for (Integer tagId: postDTO.getTagIds()) {
                Tag currentTag = tagRepository.getById(tagId);
                tags.add(currentTag);
            }
        }
        post.setTags(tags);
    }

    public PostVote VoteTypeToVote(int postId, String voteType, int userId) {
        Vote vote = voteRepository.getByVoteType(VoteType.valueOf(voteType.toUpperCase(Locale.ROOT)));
        PostVote postVote = new PostVote();
        postVote.setPostId(postId);
        postVote.setVote(vote);
        postVote.setUserId(userId);
        return postVote;
    }
}
