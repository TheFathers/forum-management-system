package com.telerikacademy.web.forum.utils.mapper;

import com.telerikacademy.web.forum.models.Comment;
import com.telerikacademy.web.forum.models.User;
import com.telerikacademy.web.forum.models.dto.CommentDTO;
import com.telerikacademy.web.forum.models.dto.CommentOutDTO;
import com.telerikacademy.web.forum.services.contracts.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CommentMapper {

    private final CommentService commentService;

    @Autowired
    public CommentMapper(CommentService commentService) {
        this.commentService = commentService;
    }

    public Comment commentDtoToComment(CommentDTO commentDTO, User editor, int postId) {
        Comment comment = new Comment();
        dtoToComment(commentDTO, comment, editor, postId);
        return comment;
    }

    public Comment commentDtoToCommentUpdate(CommentDTO commentDTO, int commentId, User editor, int postId) {
        Comment comment = commentService.getById(commentId);
        dtoToComment(commentDTO, comment, editor, postId);
        return comment;
    }

    public Comment commentDtoToCommentUpdate(CommentDTO commentDTO, int commentId) {
        Comment comment = commentService.getById(commentId);
        comment.setContent(commentDTO.getContent());
        return comment;
    }

    private void dtoToComment(CommentDTO commentDTO, Comment comment, User editor, int postId) {
        comment.setContent(commentDTO.getContent());
        comment.setPostId(postId);
        comment.setUserId(editor.getUserId());
    }

    public CommentOutDTO commentToCommentDto(Comment comment) {
        CommentOutDTO commentOutDTO = new CommentOutDTO();
        commentOutDTO.setCommentId(comment.getCommentId());
        commentOutDTO.setContent(comment.getContent());
        return commentOutDTO;
    }
}
