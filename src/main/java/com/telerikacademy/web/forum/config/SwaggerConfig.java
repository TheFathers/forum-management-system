package com.telerikacademy.web.forum.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    /**
     * Open on -> http://localhost:8080/swagger-ui/
     */

    @Bean
    public Docket postsApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .paths(PathSelectors.any())
                .apis(RequestHandlerSelectors.basePackage("com.telerikacademy.web.forum"))
                .paths(regex("/api.*"))
                .build()
                .apiInfo(apiDetails())
                .securityContexts(List.of(securityContext()))
                .securitySchemes(List.of(apiKey()));
    }

    private ApiKey apiKey() {
        return new ApiKey("JWT", "Authorization", "header");
    }

    private SecurityContext securityContext() {
        return SecurityContext
                .builder()
                .securityReferences(defaultAuth())
                .build();
    }

    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return List.of(new SecurityReference("JWT", authorizationScopes));
    }

    private ApiInfo apiDetails() {
        return new ApiInfoBuilder()
                .title("The Fathers Forum")
                .description("  Everything you need to know about childcare, from dads perspective.\n\n" +
                        "   1. Users\n" +
                        "      •\tCRUD operations (*must*)\n" +
                        "      •\tSearch by username, email, or display name (*must*)\n" +
                        "      •\tFilter and sort certain users’ posts (*must*)\n" +
                        "      •\tFilter all posts by tag/s (*could*)\n" +
                        "\n" +
                        "   2. Admin\n" +
                        "      •\tMake other users admin (*must*)\n" +
                        "      •\tDelete posts (*must*)\n" +
                        "      •\tBlock/unblock user (*must*)\n" +
                        "\n" +
                        "   3. Posts\n" +
                        "      •\tCRUD operations (*must*)\n" +
                        "      •\tComment (*must*)\n" +
                        "      •\tList and edit user’s own posts (*must*)\n" +
                        "      •\tComment on and like other users' posts. (*must*)\n" +
                        "\n" +
                        "   4. Tags – optional\n" +
                        "      •\tCRUD operations (*could*)\n")
                .termsOfServiceUrl("https://gitlab.com/t4667/forum-management-system")
                .license("Developed by: Chavdar Dimitrov and Ivaylo Stavrev")
                .licenseUrl("https://gitlab.com/t4667/forum-management-system")
                .version("0.0.1")
                .build();
    }

}
