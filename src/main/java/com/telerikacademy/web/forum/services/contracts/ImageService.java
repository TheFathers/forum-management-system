package com.telerikacademy.web.forum.services.contracts;

import com.telerikacademy.web.forum.models.attributes.Image;

public interface ImageService {

    Image getByUserId(int userId);

    void save(Image image);

    void update(Image image);
}
