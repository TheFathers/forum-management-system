package com.telerikacademy.web.forum.services.contracts;

import com.telerikacademy.web.forum.models.Comment;
import com.telerikacademy.web.forum.models.User;

import java.util.List;

public interface CommentService {

    List<Comment> getByPostId(int postId);

    Comment getById(int commentId);

    void create(Comment comment, User editor);

    void update(Comment comment, User user);

    void delete(int commentId, User user);
}
