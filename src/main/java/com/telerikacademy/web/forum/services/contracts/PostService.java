package com.telerikacademy.web.forum.services.contracts;

import com.telerikacademy.web.forum.models.Post;
import com.telerikacademy.web.forum.models.User;
import com.telerikacademy.web.forum.models.attributes.PostVote;
import com.telerikacademy.web.forum.models.attributes.Tag;
import com.telerikacademy.web.forum.models.enums.PostSortOptions;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface PostService {

    long getPostsCount();

    List<Post> getMostRecent();

    List<Post> getMostCommented();

    List<Post> getAll();

    List<Post> search(Optional<String> search, User user);

    List<Post> filter(Optional<String> keyword, Optional<String> sortBy);

    List<Post> mvcFilter(Optional<Integer> styleId, Optional<PostSortOptions> sortOptions);

    Post getById(int postId);

    void create(User user, Post post);

    void update(int postId, Post post, User user);

    void delete(int postId, User user);

    void addTags(User editor, int postId, List<String> tagNames);

    void userTagUpdate(String tagName, int tagId, int postId, User editor);

    void adminTagUpdate(String tagName, int tagId, User editor);

    void removeExistingTag(int tagId, Set<Tag> postTags);

    void createTags(List<String> tags);

    void addTagsToPost(List<String> tagNames, Post post);

    void addTagsToPostMvc(List<Integer> tagIds, int postId);

    void userTagDelete(int tagId, int postId, User editor);

    void adminTagDelete(int tagId, User admin);

    void upVote(int postId, User user, PostVote vote);

    void deleteVote(int postId, User user);

}
