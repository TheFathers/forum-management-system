package com.telerikacademy.web.forum.services;

import com.telerikacademy.web.forum.exceptions.EntityNotFoundException;
import com.telerikacademy.web.forum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.forum.models.Comment;
import com.telerikacademy.web.forum.models.Post;
import com.telerikacademy.web.forum.models.User;
import com.telerikacademy.web.forum.models.attributes.PostVote;
import com.telerikacademy.web.forum.models.attributes.Tag;
import com.telerikacademy.web.forum.models.attributes.UserType;
import com.telerikacademy.web.forum.models.enums.PostSortOptions;
import com.telerikacademy.web.forum.repositories.contracts.CommentRepository;
import com.telerikacademy.web.forum.repositories.contracts.PostRepository;
import com.telerikacademy.web.forum.repositories.contracts.TagRepository;
import com.telerikacademy.web.forum.repositories.contracts.PostVoteRepository;
import com.telerikacademy.web.forum.services.contracts.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class PostServiceImpl implements PostService {

    public static final String GET_USERS_ADMIN_ERROR_MESSAGE = "Admins can only filter Posts.";
    public static final String CREATE_POST_ERROR_MESSAGE = "Only active users can create Posts.";
    public static final String VOTE_POST_ERROR_MESSAGE = "Only active users can vote.";
    public static final String DELETE_VOTE_POST_ERROR_MESSAGE = "Only active users can remove its own votes.";
    public static final String UPDATE_POST_ERROR_MESSAGE = "Only active post owners can edit post/s.";
    public static final String DELETE_POST_ERROR_MESSAGE = "Only admins or post owners can delete post/s.";
    private static final String UPDATE_TAGS_ERROR_MESSAGE = "Only active post owners or admins can edit tags.";
    private static final String DELETE_TAGS_ERROR_MESSAGE = "Only active post owners or admins can remove tags.";
    public static final String ADMIN_USER_ROLE = "ADMIN";

    private final PostRepository postRepository;
    private final CommentRepository commentRepository;
    private final TagRepository tagRepository;
    private final PostVoteRepository voteRepository;


    @Autowired
    public PostServiceImpl(PostRepository postRepository, CommentRepository commentRepository, TagRepository tagRepository, PostVoteRepository voteRepository) {
        this.postRepository = postRepository;
        this.commentRepository = commentRepository;
        this.tagRepository = tagRepository;
        this.voteRepository = voteRepository;
    }

    @Override
    public long getPostsCount() {
        return postRepository.getPostCount();
    }

    @Override
    public List<Post> getMostRecent() {
        return postRepository.getMostRecent();
    }

    @Override
    public List<Post> getMostCommented() {
        return postRepository.getMostCommented();
    }

    @Override
    public List<Post> getAll() {
        return postRepository.getAll();
    }

    @Override
    public List<Post> search(Optional<String> search, User loggedInUser) {

        if (isNotAdmin(loggedInUser)) {
            return postRepository.getAll();
        } else {
            return postRepository.search(search);
        }

    }

    @Override
    public List<Post> filter(Optional<String> keyword, Optional<String> sortBy) {
        return postRepository.filter(keyword, sortBy);
    }

    @Override
    public List<Post> mvcFilter(Optional<Integer> styleId, Optional<PostSortOptions> sortOptions) {
        return postRepository.mvcFilter(styleId, sortOptions);
    }

    @Override
    public Post getById(int postId) {
        return postRepository.getById(postId);
    }

    @Override
    public void create(User user, Post post) {
        if (isBlocked(user)){
            throw new UnauthorizedOperationException(CREATE_POST_ERROR_MESSAGE);
        }
        postRepository.create(post);
    }

    @Override
    public void addTags(User editor, int postId, List<String> tagNames) {
        Post post = postRepository.getById(postId);
        if (isBlocked(editor) || (isNotAdmin(editor) && isNotOwner(post.getUser().getUserId(), editor)))
            throw new UnauthorizedOperationException(UPDATE_TAGS_ERROR_MESSAGE);

        createTags(tagNames);
        addTagsToPost(tagNames, post);
    }

    @Override
    public void userTagUpdate(String tagName, int tagId, int postId, User editor) {
        Post post = postRepository.getById(postId);
        if (isBlocked(editor) || isNotOwner(post.getUser().getUserId(), editor))
            throw new UnauthorizedOperationException(UPDATE_TAGS_ERROR_MESSAGE);
        Set<Tag> postTags = post.getTags();
        throwExceptionIfTagNotConnectedToPost(tagId, postTags);

        removeExistingTag(tagId, postTags);
        createTags(List.of(tagName));
        addTagsToPost(List.of(tagName), post);
    }

    @Override
    public void adminTagUpdate(String tagName, int tagId, User editor) {
        if (isBlocked(editor) || isNotAdmin(editor))
            throw new UnauthorizedOperationException(UPDATE_TAGS_ERROR_MESSAGE);
        Tag existingTag = tagRepository.getById(tagId);
        existingTag.setTagName(tagName);
        tagRepository.update(existingTag);
    }

    @Override
    public void userTagDelete(int tagId, int postId, User editor) {
        Post post = postRepository.getById(postId);
        if (isBlocked(editor) || isNotOwner(post.getUser().getUserId(), editor))
            throw new UnauthorizedOperationException(DELETE_TAGS_ERROR_MESSAGE);
        throwExceptionIfTagNotConnectedToPost(tagId, post.getTags());

        tagRepository.userTagDelete(tagId, postId);
    }

    @Override
    public void adminTagDelete(int tagId, User admin) {
        if (isBlocked(admin) || isNotAdmin(admin))
            throw new UnauthorizedOperationException(DELETE_TAGS_ERROR_MESSAGE);
        tagRepository.adminTagDelete(tagId);
    }

    @Override
    public void upVote(int postId, User user, PostVote postVote) {
        if (isBlocked(user)){
            throw new UnauthorizedOperationException(VOTE_POST_ERROR_MESSAGE);
        }
        Optional<PostVote> existingPostVote = voteRepository.getByUserAndPost(postId, user.getUserId());
        if (existingPostVote.isPresent()) {
            existingPostVote.get().setVote(postVote.getVote());
            voteRepository.update(existingPostVote.get());
        } else {
            voteRepository.create(postVote);
        }
    }

    @Override
    public void deleteVote(int postId, User user) {
        if (isBlocked(user)){
            throw new UnauthorizedOperationException(DELETE_VOTE_POST_ERROR_MESSAGE);
        }
        Optional<PostVote> existingPostVote = voteRepository.getByUserAndPost(postId, user.getUserId());
        if (existingPostVote.isPresent()) {
            voteRepository.delete(existingPostVote.get());
        } else {
            throw new EntityNotFoundException("Post", "vote of user with id", String.valueOf(user.getUserId()));
        }
    }

    @Override
    public void update(int postId, Post post, User editor) {
        if (isBlocked(editor) || isNotOwner(post.getUser().getUserId(), editor)){
            throw new UnauthorizedOperationException(UPDATE_POST_ERROR_MESSAGE);
        }
        postRepository.update(post);
    }

    @Override
    public void delete(int postId, User editor) {
        Post post = getById(postId);
        if (isBlocked(editor) || (isNotOwner(post.getUser().getUserId(), editor) && isNotAdmin(editor))){
            throw new UnauthorizedOperationException(DELETE_POST_ERROR_MESSAGE);
        }
        post.getTags().clear();
        Set<Comment> comments = post.getComments();
        if (!comments.isEmpty()) comments.forEach(commentRepository :: delete);
        List<PostVote> postVotes = voteRepository.getByPostId(postId);
        if (!postVotes.isEmpty()) postVotes.forEach(voteRepository::delete);
        postRepository.delete(post);
    }

    @Override
    public void removeExistingTag(int tagId, Set<Tag> postTags) {
        postTags.removeIf(postTag -> postTag.getTagId() == tagId);
    }

    @Override
    public void createTags(List<String> tags) {
        for (String tag : tags) {
            Optional<Tag> existingTag = tagRepository.getByName(tag);
            if (existingTag.isEmpty()) tagRepository.create(new Tag(tag));
        }
    }

    @Override
    public void addTagsToPost(List<String> tagNames, Post post) {
        for (String tagName : tagNames) {
            Optional<Tag> currentTag = tagRepository.getByName(tagName);
            currentTag.ifPresent(tag -> post.getTags().add(tag));
        }
        postRepository.update(post);
    }

    @Override
    public void addTagsToPostMvc(List<Integer> tagIds, int postId) {
        Post post = getById(postId);
        for (Integer tagId : tagIds) {
            Tag currentTag = tagRepository.getById(tagId);
            post.getTags().add(currentTag);
        }
        postRepository.update(post);
    }

    private boolean isBlocked(User editor){
        return editor.isBlocked();
    }

    private boolean isNotOwner(int ownerId, User editor) {
        return ownerId != editor.getUserId();
    }

    private boolean isNotAdmin(User editor) {
        List<String> userRoles = new ArrayList<>();
        editor.getUserTypes().forEach(userType -> userRoles.add(userType.getUserRole()));
        return !userRoles.contains(ADMIN_USER_ROLE);
    }

    private void throwExceptionIfTagNotConnectedToPost(int tagId, Set<Tag> postTags) {
        boolean areTagAndPostConnected = false;
        for (Tag postTag : postTags) {
            if (postTag.getTagId() == tagId) {
                areTagAndPostConnected = true;
                break;
            }
        }
        if (!areTagAndPostConnected) throw new EntityNotFoundException("Post", "tag with id", String.valueOf(tagId));
    }
}
