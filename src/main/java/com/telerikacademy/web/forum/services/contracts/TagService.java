package com.telerikacademy.web.forum.services.contracts;

import com.telerikacademy.web.forum.models.attributes.Tag;

import java.util.List;

public interface TagService {
    List<Tag> getAll();
}
