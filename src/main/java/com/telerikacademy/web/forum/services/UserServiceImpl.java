package com.telerikacademy.web.forum.services;

import com.telerikacademy.web.forum.exceptions.DuplicateEntityException;
import com.telerikacademy.web.forum.exceptions.EntityNotFoundException;
import com.telerikacademy.web.forum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.forum.models.User;
import com.telerikacademy.web.forum.models.attributes.Phone;
import com.telerikacademy.web.forum.models.attributes.UserType;
import com.telerikacademy.web.forum.repositories.contracts.PhoneRepository;
import com.telerikacademy.web.forum.repositories.contracts.UserRepository;
import com.telerikacademy.web.forum.repositories.contracts.UserTypeRepository;
import com.telerikacademy.web.forum.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    public static final String GET_USERS_ADMIN_ERROR_MESSAGE = "Admins can only filter users.";
    public static final String UPDATE_USER_ERROR_MESSAGE = "User can only change its own user details.";
    public static final String NO_PERMISSION = "No permission to see user with ID %d";
    public static final String PROMOTE_USER_ERROR_MESSAGE = "Admins can only promote users to admins.";
    public static final String DEMOTE_USER_ERROR_MESSAGE = "Owners can only demote themself to user.";
    public static final String ADD_PHONE_ERROR_MESSAGE = "Admins can only add phones to themself.";
    public static final String REMOVE_PHONE_ERROR_MESSAGE = "Admins can only remove phone of themself.";
    public static final String ADMIN_USER_ROLE = "ADMIN";

    private final UserRepository userRepository;
    private final UserTypeRepository userTypeRepository;
    private final PhoneRepository phoneRepository;

    @Autowired
    public UserServiceImpl(UserRepository repository, UserTypeRepository userTypeRepository, PhoneRepository phoneRepository) {
        this.userRepository = repository;
        this.userTypeRepository = userTypeRepository;
        this.phoneRepository = phoneRepository;
    }

    @Override
    public long getActiveUsersCount() {
        return userRepository.getActiveUsersCount();
    }

    @Override
    public List<User> search(Optional<String> search, User user) {
        if (isBlocked(user) || isNotAdmin(user)) throw new UnauthorizedOperationException(GET_USERS_ADMIN_ERROR_MESSAGE);
        return userRepository.search(search);
    }

    @Override
    public User getById(User loggedUserId, int userId) {
        if (isBlocked(loggedUserId) || (isNotSameUser(loggedUserId, userId) && isNotAdmin(loggedUserId)))
            throw new UnauthorizedOperationException(String.format(NO_PERMISSION,userId));

        return userRepository.getById(userId);
    }

    @Override
    public User getById(int userId) {
        return userRepository.getById(userId);
    }

    @Override
    public User getByUsername(String username) {
        return userRepository.getByUsername(username);
    }

    @Override
    public void create(User user) {
        if (emailExists(user)) throw new DuplicateEntityException("User", "email", user.getEmail());
        if (usernameExists(user)) throw new DuplicateEntityException("User", "username", user.getUsername());
        if (newUserIsAdmin(user)) throw new UnauthorizedOperationException("New user can be only with role User.");
        userRepository.create(user);
    }



    @Override
    public void blockUserInverter(User loggedUser, int userId) {
        if (isBlocked(loggedUser) || isNotAdmin(loggedUser)){
            throw new UnauthorizedOperationException(String.format(NO_PERMISSION,userId));
        }
        userRepository.blockUserInverter(userId);
    }

    @Override
    public void update(User user, User updatedUser) {
        if (isBlocked(user) || isNotSameUser(user, updatedUser.getUserId()))
            throw new UnauthorizedOperationException(UPDATE_USER_ERROR_MESSAGE);
        if (usernameExists(updatedUser))
            throw new DuplicateEntityException("Username", "username", updatedUser.getUsername());
        if (emailExists(updatedUser))
            throw new DuplicateEntityException("User", "email", updatedUser.getEmail());

        userRepository.update(updatedUser);
    }

    @Override
    public void promote(User loggedInUser, int userId) {
        User userToBeAdmin = userRepository.getById(userId);
        if(isBlocked(userToBeAdmin) || isNotAdmin(loggedInUser))
            throw new UnauthorizedOperationException(PROMOTE_USER_ERROR_MESSAGE);

        UserType userType = userTypeRepository.getUserType(ADMIN_USER_ROLE);
        userToBeAdmin.getUserTypes().add(userType);
        userRepository.update(userToBeAdmin);
    }

    @Override
    public void demote(User loggedInUser, int userId) {
        if (isBlocked(loggedInUser) || isNotSameUser(loggedInUser, userId) || isNotAdmin(loggedInUser))
            throw new UnauthorizedOperationException(DEMOTE_USER_ERROR_MESSAGE);
        Optional<Phone> existingPhone = phoneRepository.getByUserId(userId);
        existingPhone.ifPresent(phoneRepository::delete);

        UserType userType = userTypeRepository.getUserType(ADMIN_USER_ROLE);
        loggedInUser.getUserTypes().remove(userType);
        userRepository.update(loggedInUser);
    }

    @Override
    public void addPhone(User loggedInUser, int userId, String phone) {
        if (isBlocked(loggedInUser) || isNotSameUser(loggedInUser, userId) || isNotAdmin(loggedInUser))
            throw new UnauthorizedOperationException(ADD_PHONE_ERROR_MESSAGE);
        Phone phoneToBe = new Phone(userId, phone);
        Optional<Phone> existingPhone = phoneRepository.getByUserId(userId);
        if (existingPhone.isPresent()){
            if (!existingPhone.get().getPhone().equals(phone)){
                phoneRepository.create(phoneToBe);
            } else {
                throw new DuplicateEntityException("Phone", "phone number", phoneToBe.getPhone());
            }
        } else {
            phoneRepository.create(phoneToBe);
        }
    }

    @Override
    public void removePhone(User loggedInUser, int userId) {
        if (isBlocked(loggedInUser) || isNotSameUser(loggedInUser, userId) || isNotAdmin(loggedInUser))
            throw new UnauthorizedOperationException(REMOVE_PHONE_ERROR_MESSAGE);
        Optional<Phone> existingPhone = phoneRepository.getByUserId(userId);

        existingPhone.ifPresent(phoneRepository::delete);
    }

    public boolean newUserIsAdmin(User newUser) {
        List<String> userRoles = new ArrayList<>();
        newUser.getUserTypes().forEach(userType -> userRoles.add(userType.getUserRole()));
        return userRoles.contains(ADMIN_USER_ROLE);
    }

    public boolean isBlocked(User loggedInUser){return loggedInUser.isBlocked();}

    public boolean isNotAdmin(User loggedInUser) {
        List<String> userRoles = new ArrayList<>();
        loggedInUser.getUserTypes().forEach(userType -> userRoles.add(userType.getUserRole()));
        return !userRoles.contains(ADMIN_USER_ROLE);
    }

    public boolean isNotSameUser(User userToBeAuthorized, int userId) {
        return userToBeAuthorized.getUserId() != userId;
    }

    public boolean emailExists(User userToBe) {
        try {
            User existingUser = userRepository.getByEmail(userToBe.getEmail());
            if (existingUser.getUserId() == userToBe.getUserId()) {
                return false;
            }
        } catch (EntityNotFoundException e) {
            return false;
        }
        return true;
    }

    public boolean usernameExists(User userToBe) {
        try {
            User existingUser = userRepository.getByUsername(userToBe.getUsername());
            if (existingUser.getUserId() == userToBe.getUserId()){
                return false;
            }
        } catch (EntityNotFoundException e) {
            return false;
        }
        return true;
    }
}

