package com.telerikacademy.web.forum.services;

import com.telerikacademy.web.forum.models.attributes.PostVote;
import com.telerikacademy.web.forum.repositories.contracts.PostVoteRepository;
import com.telerikacademy.web.forum.services.contracts.PostVoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PostVoteServiceImpl implements PostVoteService{

    private final PostVoteRepository postVoteRepository;

    @Autowired
    public PostVoteServiceImpl(PostVoteRepository postVoteRepository) {
        this.postVoteRepository = postVoteRepository;
    }

    @Override
    public List<PostVote> getAll() {
        return postVoteRepository.getAll();
    }

    @Override
    public Optional<PostVote> getByUserAndPost(int postId, int userId) {
        return postVoteRepository.getByUserAndPost(postId, userId);
    }

    @Override
    public void create(PostVote postVote) {
        postVoteRepository.create(postVote);
    }
}
