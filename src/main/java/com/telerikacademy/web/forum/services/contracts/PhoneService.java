package com.telerikacademy.web.forum.services.contracts;

import com.telerikacademy.web.forum.models.attributes.Phone;

import java.util.Optional;

public interface PhoneService {

    Optional<Phone> getByUserId(int userId);

    void update(Phone phoneToBe);

    void create(Phone phoneToBe);

    void delete(Phone phone);
}
