package com.telerikacademy.web.forum.services;

import com.telerikacademy.web.forum.models.attributes.Tag;
import com.telerikacademy.web.forum.repositories.contracts.TagRepository;
import com.telerikacademy.web.forum.services.contracts.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagServiceImpl implements TagService {

    private final TagRepository tagRepository;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public List<Tag> getAll() {
        return tagRepository.getAll();
    }
}
