package com.telerikacademy.web.forum.services.contracts;

import com.telerikacademy.web.forum.models.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    long getActiveUsersCount();

    List<User> search(Optional<String> search, User user);

    User getById(User loggedInUser, int userId);

    User getById(int userId);

    User getByUsername(String username);

    void create(User user);

    void blockUserInverter(User loggedUser, int userId);

    void update(User userToBeUpdated, User userUpdate);

    void promote(User loggedInUser, int userId);

    void demote(User loggedInUser, int userId);

    void addPhone(User loggedInUser, int userId, String phone);

    void removePhone(User loggedInUser, int userId);
}
