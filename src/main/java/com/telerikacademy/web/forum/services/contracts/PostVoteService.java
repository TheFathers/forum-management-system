package com.telerikacademy.web.forum.services.contracts;

import com.telerikacademy.web.forum.models.attributes.PostVote;

import java.util.List;
import java.util.Optional;

public interface PostVoteService {

    List<PostVote> getAll();

    Optional<PostVote> getByUserAndPost(int postId, int userId);

    void create(PostVote postVote);

}
