package com.telerikacademy.web.forum.services;

import com.telerikacademy.web.forum.models.attributes.Image;
import com.telerikacademy.web.forum.repositories.contracts.ImageRepository;
import com.telerikacademy.web.forum.services.contracts.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImageServiceImpl implements ImageService {

    private final ImageRepository imageRepository;

    @Autowired
    public ImageServiceImpl(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }


    @Override
    public Image getByUserId(int userId) {
        return imageRepository.getByUserId(userId);
    }

    @Override
    public void save(Image image) {
        imageRepository.save(image);
    }

    @Override
    public void update(Image image) {
        imageRepository.update(image);
    }
}
