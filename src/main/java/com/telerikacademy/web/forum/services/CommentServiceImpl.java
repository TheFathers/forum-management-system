package com.telerikacademy.web.forum.services;

import com.telerikacademy.web.forum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.forum.models.Comment;
import com.telerikacademy.web.forum.models.User;
import com.telerikacademy.web.forum.repositories.contracts.CommentRepository;
import com.telerikacademy.web.forum.services.contracts.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    public static final String CREATE_COMMENT_ERROR_MESSAGE = "Only active user can comment.";
    public static final String UPDATE_COMMENT_ERROR_MESSAGE = "Only admin or post owner can modify comment/s.";
    public static final String DELETE_COMMENT_ERROR_MESSAGE = "Only admin or post owner can delete comment/s.";
    public static final String ADMIN_USER_ROLE = "ADMIN";

    private final CommentRepository commentRepository;

    @Autowired
    public CommentServiceImpl(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    @Override
    public List<Comment> getByPostId(int postId) {
        return commentRepository.getByPostId(postId);
    }

    @Override
    public Comment getById(int commentId) {
        return commentRepository.getById(commentId);
    }

    @Override
    public void create(Comment comment, User editor) {
        if (isBlocked(editor)) throw new UnauthorizedOperationException(CREATE_COMMENT_ERROR_MESSAGE);
        commentRepository.create(comment);
    }

    @Override
    public void update(Comment comment, User editor) {
        if (isBlocked(editor) || isNotOwner(comment.getUserId(), editor)) {
            throw new UnauthorizedOperationException(UPDATE_COMMENT_ERROR_MESSAGE);
        }
        commentRepository.update(comment);
    }

    @Override
    public void delete(int commentId, User editor) {
        Comment comment = getById(commentId);
        if (isBlocked(editor) || (isNotAdmin(editor) && isNotOwner(comment.getUserId(), editor)))
            throw new UnauthorizedOperationException(DELETE_COMMENT_ERROR_MESSAGE);
        commentRepository.delete(comment);
    }

    private boolean isBlocked(User editor){
        return editor.isBlocked();
    }

    private boolean isNotOwner(int commentOwnerId, User editor) {
        return commentOwnerId != editor.getUserId();
    }

    private boolean isNotAdmin(User editor) {
        List<String> userRoles = new ArrayList<>();
        editor.getUserTypes().forEach(userType -> userRoles.add(userType.getUserRole()));
        return !userRoles.contains(ADMIN_USER_ROLE);
    }
}
