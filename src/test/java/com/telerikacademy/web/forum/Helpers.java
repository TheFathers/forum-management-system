package com.telerikacademy.web.forum;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.telerikacademy.web.forum.models.Comment;
import com.telerikacademy.web.forum.models.Post;
import com.telerikacademy.web.forum.models.User;
import com.telerikacademy.web.forum.models.attributes.*;
import com.telerikacademy.web.forum.models.dto.CommentDTO;
import com.telerikacademy.web.forum.models.dto.PostDTO;
import com.telerikacademy.web.forum.models.enums.VoteType;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Helpers {

    public static User createMockUser() {
        return createMockUser("USER");
    }

    public static User createMockAdmin() {
        return createMockUser("ADMIN");
    }

    public static User createBlockedUser() {
        User user = createMockUser();
        user.setBlocked(true);
        return user;
    }

    public static User createBlockedAdmin() {
        User user = createMockAdmin();
        user.setBlocked(true);
        return user;
    }

    public static User createNotPostOwner() {
        User user = createMockUser();
        user.setUserId(2);
        return user;
    }

    private static User createMockUser(String role) {
        var mockUser = new User();
        mockUser.setUserId(1);
        mockUser.setEmail("mock@user.com");
        mockUser.setUsername("MockUsername");
        mockUser.setFirstName("MockFirstName");
        mockUser.setLastName("MockLastName");
        mockUser.setPassword("MockPassword");
        mockUser.setBlocked(false);
        mockUser.setUserTypes(getUserTypes(role));
        return mockUser;
    }

    public static Post createMockPost() {
        var mockPost = new Post();
        mockPost.setPostId(1);
        mockPost.setTitle("TestTitle");
        mockPost.setContent("TestContent");
        mockPost.setCreationTime(LocalDateTime.now());
        mockPost.setUser(createMockUser());
        mockPost.setTags(getTagSet());
        mockPost.setComments(getCommentSet());
        mockPost.setVotes(getVotesSet());

        return mockPost;
    }



    public static UserType createMockRole(String role) {
        var mockRole = new UserType();
        mockRole.setUserTypeId(2);
        mockRole.setUserRole(role);
        return mockRole;
    }

    public static Comment createMockComment() {
        var mockComment = new Comment();
        mockComment.setCommentId(1);
        mockComment.setContent("Content");
        mockComment.setPostId(1);
        mockComment.setUserId(1);
        return mockComment;
    }


    public static Tag createMockTag() {
        var mockTag = new Tag();
        mockTag.setTagId(1);
        mockTag.setTagName("Tag");
        return mockTag;
    }

    public static List<String> createMockTagList() {
        List<String> mockTagList = new ArrayList<>();
        mockTagList.add(createMockTag().getTagName());
        return mockTagList;
    }


    public static Vote createMockVote() {
        var mockVote = new Vote();
        mockVote.setVoteId(1);
        mockVote.setVoteType(VoteType.LIKE);
        return mockVote;
    }

    public static PostVote createMockPostVote() {
        var mockPostVote = new PostVote();
        mockPostVote.setPostVoteId(1);
        mockPostVote.setPostId(1);
        mockPostVote.setUserId(1);
        mockPostVote.setVote(createMockVote());
        return mockPostVote;
    }

    public static UserType createMockUserTypeAdmin() {
        var mockUserType = new UserType();
        mockUserType.setUserTypeId(1);
        mockUserType.setUserRole("ADMIN");
        return mockUserType;
    }

    public static Phone createMockPhone() {
        var mockPhone = new Phone();
        mockPhone.setUser(1);
        mockPhone.setPhone("0858127212");
        return mockPhone;
    }

    public static Phone createMockPhone(String phone) {
        var mockPhone = new Phone();
        mockPhone.setUser(1);
        mockPhone.setPhone(phone);
        return mockPhone;
    }

    public static PostDTO createValidPostDto() {
        PostDTO dto = new PostDTO();
        dto.setTitle("Title");
        dto.setContent("content");
        return dto;
    }

    public static CommentDTO createValidCommentDto() {
        return new CommentDTO();
    }

    public static Set<Tag> getTagSet(){
        Set<Tag> tags = new HashSet<>();
        tags.add(createMockTag());
        return tags;
    }

    public static Set<Comment> getCommentSet(){
        Set<Comment> comments = new HashSet<>();
        comments.add(createMockComment());
        return comments;
    }

    private static List<Vote> getVotesSet() {
        List<Vote> votes = new ArrayList<>();
        votes.add(createMockVote());
        return votes;
    }

    private static Set<UserType> getUserTypes(String role) {
        Set<UserType> userTypes = new HashSet<>();
        UserType userType = new UserType();
        userType.setUserRole(role);
        userTypes.add(userType);
        return userTypes;
    }

    /**
     * Accepts an object and returns the string field object.
     * Useful when you need to pass a body to an HTTP request.
     */
    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
