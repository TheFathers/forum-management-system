package com.telerikacademy.web.forum.services;

import com.telerikacademy.web.forum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.forum.models.Comment;
import com.telerikacademy.web.forum.models.User;
import com.telerikacademy.web.forum.repositories.contracts.CommentRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.telerikacademy.web.forum.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class CommentServiceTests {

    @Mock CommentRepository mockCommentRepository;

    @InjectMocks CommentServiceImpl service;

    @Test
    void getByPostId_should_callRepository() {
        // Arrange
        Mockito.when(mockCommentRepository.getByPostId(Mockito.anyInt())).thenReturn(new ArrayList<>());

        // Act
        service.getByPostId(Mockito.anyInt());

        // Assert
        Mockito.verify(mockCommentRepository, Mockito.times(1))
                .getByPostId(Mockito.anyInt());
    }

    @Test
    void getById_should_callRepository() {
        // Arrange
        Mockito.when(mockCommentRepository.getById(Mockito.anyInt())).thenReturn(createMockComment());

        // Act
        service.getById(Mockito.anyInt());

        // Assert
        Mockito.verify(mockCommentRepository, Mockito.times(1))
                .getById(Mockito.anyInt());
    }

    @Test
    public void getById_should_returnComment_when_matchExist() {
        // Arrange
        Comment mockComment = createMockComment();
        Mockito.when(mockCommentRepository.getById(mockComment.getCommentId())).thenReturn(mockComment);
        // Act
        Comment result = service.getById(mockComment.getCommentId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockComment.getCommentId(), result.getCommentId()),
                () -> Assertions.assertEquals(mockComment.getPostId(), result.getPostId()),
                () -> Assertions.assertEquals(mockComment.getContent(), result.getContent()),
                () -> Assertions.assertEquals(mockComment.getUserId(), result.getUserId())
        );
    }

    @Test
    public void create_should_throw_when_userIsBlocked() {
        // Arrange
        Comment mockComment = createMockComment();
        User mockUser = createBlockedUser();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.create(mockComment, mockUser));
    }

    @Test
    public void create_should_callRepository_when_userIsActive() {
        // Arrange
        Comment mockComment = createMockComment();
        User mockUser = createMockUser();

        // Act
        service.create(mockComment, mockUser);

        // Assert
        Mockito.verify(mockCommentRepository, Mockito.times(1)).create(mockComment);
    }

    @Test
    public void update_should_throw_when_userIsBlocked() {
        // Arrange
        Comment mockComment = createMockComment();
        User mockUser = createBlockedUser();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.update(mockComment, mockUser));
    }

    @Test
    public void update_should_throw_when_userIsNotOwner() {
        // Arrange
        Comment mockComment = createMockComment();
        mockComment.setUserId(2);
        User mockUser = createMockUser();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.update(mockComment, mockUser));
    }

    @Test
    public void update_should_callRepository_when_userIsActiveOwner() {
        // Arrange
        Comment mockComment = createMockComment();
        User mockUser = createMockUser();

        // Act
        service.update(mockComment, mockUser);

        // Assert
        Mockito.verify(mockCommentRepository, Mockito.times(1)).update(mockComment);
    }

    @Test
    public void delete_should_throw_when_userIsBlocked() {
        // Arrange
        int commentId = createMockComment().getCommentId();
        User mockUser = createBlockedUser();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.delete(commentId, mockUser));
    }

    @Test
    public void delete_should_throw_when_userIsNotOwner() {
        // Arrange
        Comment mockComment = createMockComment();
        mockComment.setUserId(2);
        int commentId = mockComment.getCommentId();
        User mockUser = createMockUser();

        Mockito.when(mockCommentRepository.getById(Mockito.anyInt())).thenReturn(mockComment);

        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.delete(commentId, mockUser));
    }

    @Test
    public void delete_should_callRepository_when_userIsActiveOwner() {
        // Arrange
        Comment mockComment = createMockComment();
        int commentId = mockComment.getCommentId();
        User mockUser = createMockUser();

        Mockito.when(mockCommentRepository.getById(Mockito.anyInt())).thenReturn(mockComment);

        // Act
        service.delete(commentId, mockUser);

        // Assert
        Mockito.verify(mockCommentRepository, Mockito.times(1)).delete(mockComment);
    }

}

