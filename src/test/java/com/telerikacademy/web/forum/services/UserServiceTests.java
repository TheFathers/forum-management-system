package com.telerikacademy.web.forum.services;

import com.telerikacademy.web.forum.exceptions.DuplicateEntityException;
import com.telerikacademy.web.forum.exceptions.EntityNotFoundException;
import com.telerikacademy.web.forum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.forum.models.User;
import com.telerikacademy.web.forum.models.attributes.Phone;
import com.telerikacademy.web.forum.models.attributes.UserType;
import com.telerikacademy.web.forum.repositories.PhoneRepositoryImpl;
import com.telerikacademy.web.forum.repositories.UserRepositoryImpl;
import com.telerikacademy.web.forum.repositories.UserTypeRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static com.telerikacademy.web.forum.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTests {

    @Mock UserRepositoryImpl mockUserRepository;
    @Mock UserTypeRepositoryImpl mockUserTypeRepository;
    @Mock PhoneRepositoryImpl mockPhoneRepository;

    @InjectMocks
    UserServiceImpl service;


    @Test
    void search_should_throw_when_userIsBlockedAdmin() {
        // Arrange
        User admin = createBlockedAdmin();

        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.search(Optional.empty(), admin));
    }

    @Test
    void search_should_throw_when_userIsNotAdmin() {
        // Arrange
        User user = createMockUser();

        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.search(Optional.empty(), user));
    }

    @Test
    void search_should_callRepository_when_theSameUserIsActiveAdmin() {
        // Arrange
        User mockAdmin = createMockAdmin();
        Mockito.when(mockUserRepository.search(Optional.empty()))
                .thenReturn(new ArrayList<>());

        // Act
        service.search(Optional.empty(), mockAdmin);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .search(Optional.empty());
    }

    @Test
    void getById_should_throw_when_userIsBlocked() {
        // Arrange
        User loggedInUser = createBlockedUser();
        int userId = loggedInUser.getUserId() + 1;

        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getById(loggedInUser, userId));
    }

    @Test
    void getById_should_throw_when_userIsNotTheSameUserAndIsNotAdmin() {
        // Arrange
        User loggedInUser = createMockUser();
        int userId = loggedInUser.getUserId() + 1;

        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getById(loggedInUser, userId));
    }

    @Test
    void getById_should_callRepository_when_sameUserIsActiveAdmin() {
        // Arrange
        User mockAdmin = createMockAdmin();
        int userId = mockAdmin.getUserId();

        // Act
        service.getById(mockAdmin, userId);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getById(userId);
    }

    @Test
    public void getById_should_returnUser_when_matchExist() {
        // Arrange
        User mockAdmin = createMockAdmin();
        User mockUser = createMockUser();
        int userId = mockUser.getUserId();
        Mockito.when(mockUserRepository.getById(Mockito.anyInt()))
                .thenReturn(mockUser);
        // Act
        User result = service.getById(mockAdmin, userId);

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getUserId(), result.getUserId()),
                () -> Assertions.assertEquals(mockUser.getUsername(), result.getUsername()),
                () -> Assertions.assertEquals(mockUser.getFirstName(), result.getFirstName()),
                () -> Assertions.assertEquals(mockUser.getLastName(), result.getLastName()),
                () -> Assertions.assertEquals(mockUser.getEmail(), result.getEmail()),
                () -> Assertions.assertEquals(mockUser.getUserTypes(), result.getUserTypes())
        );
    }


    @Test
    void getByName_should_callRepository() {
        // Arrange
        String username = createMockUser().getUsername();

        // Act
        service.getByUsername(username);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1)).getByUsername(username);
    }

    @Test
    public void getByUsername_should_returnUser_when_matchExist() {
        // Arrange
        User mockUser = createMockUser();
        String username = mockUser.getUsername();
        Mockito.when(mockUserRepository.getByUsername(Mockito.anyString())).thenReturn(mockUser);
        // Act
        User result = service.getByUsername(username);

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getUserId(), result.getUserId()),
                () -> Assertions.assertEquals(mockUser.getUsername(), result.getUsername()),
                () -> Assertions.assertEquals(mockUser.getFirstName(), result.getFirstName()),
                () -> Assertions.assertEquals(mockUser.getLastName(), result.getLastName()),
                () -> Assertions.assertEquals(mockUser.getEmail(), result.getEmail()),
                () -> Assertions.assertEquals(mockUser.getUserTypes(), result.getUserTypes())
        );
    }

    @Test
    public void create_should_throw_when_userWithSameEmailExists() {
        // Arrange
        User existingMockUser = createMockUser();
        existingMockUser.setUserId(2);
        User newMockUser = createMockUser();
        existingMockUser.setEmail(newMockUser.getEmail());

        Mockito.when(mockUserRepository.getByEmail(existingMockUser.getEmail()))
                .thenReturn(existingMockUser);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.create(newMockUser));
    }

    @Test
    public void create_should_throw_when_userWithSameUsernameExists() {
        // Arrange
        User existingMockUser = createMockUser();
        existingMockUser.setUserId(2);
        User newMockUser = createMockUser();
        existingMockUser.setEmail(newMockUser.getEmail());

        Mockito.when(mockUserRepository.getByEmail(newMockUser.getEmail()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(mockUserRepository.getByUsername(existingMockUser.getUsername()))
                .thenReturn(existingMockUser);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(newMockUser));
    }

    @Test
    public void create_should_callRepository_when_userWithSameEmailAndUsernameDoesNotExist() {
        // Arrange
        User newMockUser = createMockUser();

        Mockito.when(mockUserRepository.getByEmail(newMockUser.getEmail()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(mockUserRepository.getByUsername(newMockUser.getUsername()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        service.create(newMockUser);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .create(newMockUser);
    }

    @Test
    void blockUserInverter_should_throw_when_userIsBlockedAdmin() {
        // Arrange
        User blockedAdmin = createBlockedAdmin();
        int userId = createMockUser().getUserId();

        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.blockUserInverter(blockedAdmin, userId));
    }

    @Test
    void blockUserInverter_should_throw_when_userIsNotAdmin() {
        // Arrange
        User mockUser = createMockUser();
        int userId = mockUser.getUserId();

        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.blockUserInverter(mockUser, userId));
    }

    @Test
    void blockUserInverter_should_callRepository_when_userIsActiveAdmin() {
        // Arrange
        User admin = createMockAdmin();
        int userId = createMockUser().getUserId();

        // Act
        service.blockUserInverter(admin, userId);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .blockUserInverter(userId);
    }

    @Test
    void update_should_throw_when_userIsBlocked() {
        // Arrange
        User mockUser = createBlockedUser();

        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.update(mockUser, mockUser));
    }

    @Test
    void update_should_throw_when_usersAreNotTheSameUser() {
        // Arrange
        User mockUser = createMockUser();
        User mockUpdatedUser = new User();
        mockUpdatedUser.setUserId(2);

        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.update(mockUser, mockUpdatedUser));
    }

    @Test
    void update_should_throw_when_usernameAlreadyExist() {
        // Arrange
        User mockUser = createMockUser();
        mockUser.setUsername("DuplicateUsername");
        User anotherUser = createMockUser();
        anotherUser.setUserId(2);

        Mockito.when(mockUserRepository.getByUsername(mockUser.getUsername()))
                .thenReturn(anotherUser);

        // Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.update(mockUser, mockUser));
    }

    @Test
    void update_should_throw_when_emailAlreadyExist() {
        // Arrange
        User mockUser = createMockUser();
        mockUser.setEmail("duplicate@forum.bg");
        User anotherUser = createMockUser();
        anotherUser.setUserId(2);

        Mockito.when(mockUserRepository.getByUsername(mockUser.getUsername()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(mockUserRepository.getByEmail(mockUser.getEmail()))
                .thenReturn(anotherUser);

        // Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.update(mockUser, mockUser));
    }

    @Test
    void update_should_callRepository_when_tryingToUpdateExistingUser() {
        // Arrange
        User mockUser = createMockUser();
        User mockUpdatedUser = createMockUser();

        Mockito.when(mockUserRepository.getByUsername(Mockito.anyString()))
                .thenReturn(mockUpdatedUser);

        Mockito.when(mockUserRepository.getByEmail(Mockito.anyString()))
                .thenReturn(mockUpdatedUser);

        service.update(mockUser, mockUpdatedUser);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .update(mockUpdatedUser);
    }

    @Test
    void promote_should_throw_when_userIsBlocked() {
        // Arrange
        User mockAdmin = createMockAdmin();
        mockAdmin.setUserId(2);
        User mockUser = createBlockedUser();
        int userId = mockUser.getUserId();

        Mockito.when(mockUserRepository.getById(userId)).thenReturn(mockUser);

        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.promote(mockAdmin, userId));
    }

    @Test
    void promote_should_throw_when_userIsNotAdmin() {
        // Arrange
        User mockUser = createMockUser();
        int userId = mockUser.getUserId();

        Mockito.when(mockUserRepository.getById(userId)).thenReturn(mockUser);

        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.promote(mockUser, userId));
    }


    @Test
    void promote_should_callRepository_when_adminPromotesUser() {
        // Arrange
        User mockAdmin = createMockAdmin();
        mockAdmin.setUserId(2);
        User mockUser = createMockUser();
        int userId = mockUser.getUserId();
        UserType mockUserType = createMockUserTypeAdmin();

        Mockito.when(mockUserRepository.getById(userId)).thenReturn(mockUser);

        Mockito.when(mockUserTypeRepository.getUserType(Mockito.anyString())).thenReturn(mockUserType);

        service.promote(mockAdmin, userId);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .update(mockUser);
    }

    @Test
    void demote_should_throw_when_userIsBlocked() {
        // Arrange
        User mockAdmin = createBlockedAdmin();
        int userId = mockAdmin.getUserId();

        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.demote(mockAdmin, userId));
    }

    @Test
    void demote_should_throw_when_usersAreNotSame() {
        // Arrange
        User mockAdmin = createMockAdmin();
        int userId = mockAdmin.getUserId() + 1;

        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.demote(mockAdmin, userId));
    }

    @Test
    void demote_should_throw_when_userIsNotAdmin() {
        // Arrange
        User mockUser = createMockUser();
        int userId = mockUser.getUserId();

        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.demote(mockUser, userId));
    }

    @Test
    void demote_should_callPhoneRepository_when_adminDemotesHimself() {
        // Arrange
        User mockAdmin = createMockAdmin();
        int userId = mockAdmin.getUserId();
        Optional<Phone> optionalMockPhone = Optional.of(createMockPhone());

        Mockito.when(mockPhoneRepository.getByUserId(userId)).thenReturn(optionalMockPhone);

        service.demote(mockAdmin, userId);

        // Assert
        Mockito.verify(mockPhoneRepository, Mockito.times(1)).delete(optionalMockPhone.get());
    }

    @Test
    void demote_should_callRepository_when_adminDemotesHimself() {
        // Arrange
        User mockAdmin = createMockAdmin();
        int userId = mockAdmin.getUserId();
        UserType mockUserType = createMockUserTypeAdmin();

        Mockito.when(mockUserTypeRepository.getUserType(Mockito.anyString())).thenReturn(mockUserType);

        service.demote(mockAdmin, userId);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .update(mockAdmin);
    }

    @Test
    void addPhone_should_throw_when_userIsBlocked() {
        // Arrange
        User mockAdmin = createBlockedAdmin();
        int userId = mockAdmin.getUserId();
        Phone mockPhone = createMockPhone();

        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.addPhone(mockAdmin, userId, mockPhone.getPhone()));
    }

    @Test
    void addPhone_should_throw_when_usersAreNotSame() {
        // Arrange
        User mockAdmin = createMockAdmin();
        int userId = mockAdmin.getUserId() + 1;
        Phone mockPhone = createMockPhone();

        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.addPhone(mockAdmin, userId, mockPhone.getPhone()));
    }

    @Test
    void addPone_should_throw_when_userIsNotAdmin() {
        // Arrange
        User mockUser = createMockUser();
        int userId = mockUser.getUserId();
        Phone mockPhone = createMockPhone();

        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.addPhone(mockUser, userId, mockPhone.getPhone()));
    }

    @Test
    void addPone_should_throw_when_newPhoneIsTheSame() {
        // Arrange
        User mockAdmin = createMockAdmin();
        mockAdmin.setPhone(createMockPhone());
        int userId = mockAdmin.getUserId();
        Optional<Phone> optionalMockPhone = Optional.of(mockAdmin.getPhone());

        Mockito.when(mockPhoneRepository.getByUserId(userId)).thenReturn(optionalMockPhone);

        // Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.addPhone(mockAdmin, userId, optionalMockPhone.get().getPhone()));
    }

    @Test
    void addPhone_should_callPhoneRepository_when_userIsSameActiveAdminWithNoPhone() {
        // Arrange
        User mockAdmin = createMockAdmin();
        mockAdmin.setPhone(createMockPhone("MockitoOldPhone"));
        int userId = mockAdmin.getUserId();
        Phone mockPhone = createMockPhone("MockitoNewPhone");

        Mockito.when(mockPhoneRepository.getByUserId(userId)).thenReturn(Optional.empty());

        service.addPhone(mockAdmin, userId, mockPhone.getPhone());

        // Assert
        Mockito.verify(mockPhoneRepository, Mockito.times(1)).create(mockPhone);
    }

    @Test
    void addPhone_should_callPhoneRepository_when_userIsSameActiveAdminWithExistingPhone() {
        // Arrange
        User mockAdmin = createMockAdmin();
        mockAdmin.setPhone(createMockPhone("MockitoOldPhone"));
        int userId = mockAdmin.getUserId();
        Phone mockPhone = createMockPhone("MockitoNewPhone");

        Mockito.when(mockPhoneRepository.getByUserId(userId)).thenReturn(Optional.of(mockAdmin.getPhone()));

        service.addPhone(mockAdmin, userId, mockPhone.getPhone());

        // Assert
        Mockito.verify(mockPhoneRepository, Mockito.times(1)).create(mockPhone);
    }

    @Test
    void removePhone_should_throw_when_userIsBlocked() {
        // Arrange
        User mockAdmin = createBlockedAdmin();
        int userId = mockAdmin.getUserId();

        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.removePhone(mockAdmin, userId));
    }

    @Test
    void removePhone_should_throw_when_usersAreNotSame() {
        // Arrange
        User mockAdmin = createMockAdmin();
        int userId = mockAdmin.getUserId() + 1;

        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.removePhone(mockAdmin, userId));
    }

    @Test
    void removePhone_should_throw_when_userIsNotAdmin() {
        // Arrange
        User mockUser = createMockUser();
        int userId = mockUser.getUserId();

        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.removePhone(mockUser, userId));
    }

    @Test
    void removePhone_should_callRepository_when_adminRemovesHisPhone() {
        // Arrange
        User mockAdmin = createMockAdmin();
        mockAdmin.setPhone(createMockPhone());
        int userId = mockAdmin.getUserId();

        Mockito.when(mockPhoneRepository.getByUserId(userId)).thenReturn(Optional.of(mockAdmin.getPhone()));

        service.removePhone(mockAdmin, userId);

        // Assert
        Mockito.verify(mockPhoneRepository, Mockito.times(1))
                .delete(mockAdmin.getPhone());
    }
}
