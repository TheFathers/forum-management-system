package com.telerikacademy.web.forum.services;

import com.telerikacademy.web.forum.exceptions.EntityNotFoundException;
import com.telerikacademy.web.forum.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.forum.models.Post;
import com.telerikacademy.web.forum.models.User;
import com.telerikacademy.web.forum.models.attributes.PostVote;
import com.telerikacademy.web.forum.models.attributes.Tag;
import com.telerikacademy.web.forum.repositories.contracts.CommentRepository;
import com.telerikacademy.web.forum.repositories.contracts.PostRepository;
import com.telerikacademy.web.forum.repositories.contracts.TagRepository;
import com.telerikacademy.web.forum.repositories.contracts.PostVoteRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.telerikacademy.web.forum.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class PostServiceTests {

    @Mock PostRepository mockPostRepository;
    @Mock TagRepository mockTagRepository;
    @Mock
    PostVoteRepository mockVoteRepository;
    @Mock CommentRepository mockCommentRepository;

    @InjectMocks PostServiceImpl service;

    @Test
    void mostRecent_should_callRepository() {
        // Arrange
        Mockito.when(mockPostRepository.getMostRecent())
                .thenReturn(new ArrayList<>());

        // Act
        service.getMostRecent();

        // Assert
        Mockito.verify(mockPostRepository, Mockito.times(1))
                .getMostRecent();
    }

    @Test
    void mostCommented_should_callRepository() {
        // Arrange
        Mockito.when(mockPostRepository.getMostCommented())
                .thenReturn(new ArrayList<>());

        // Act
        service.getMostCommented();

        // Assert
        Mockito.verify(mockPostRepository, Mockito.times(1))
                .getMostCommented();
    }

    @Test
    void search_should_callRepository() {
        // Arrange
        User mockAdmin = createMockAdmin();
        Mockito.when(mockPostRepository.search(Optional.empty()))
                .thenReturn(new ArrayList<>());

        // Act
        service.search(Optional.empty(), mockAdmin);

        // Assert
        Mockito.verify(mockPostRepository, Mockito.times(1))
                .search(Optional.empty());
    }

    @Test
    void filter_should_callRepository() {
        // Arrange
        Mockito.when(mockPostRepository.filter(Optional.empty(), Optional.empty()))
                .thenReturn(new ArrayList<>());

        // Act
        service.filter(Optional.empty(), Optional.empty());

        // Assert
        Mockito.verify(mockPostRepository, Mockito.times(1))
                .filter(Optional.empty(), Optional.empty());
    }

    @Test
    public void getById_should_returnPost_when_matchExist() {
        // Arrange
        Post mockPost = createMockPost();
        Mockito.when(mockPostRepository.getById(mockPost.getPostId()))
                .thenReturn(mockPost);
        // Act
        Post result = service.getById(mockPost.getPostId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockPost.getPostId(), result.getPostId()),
                () -> Assertions.assertEquals(mockPost.getTitle(), result.getTitle()),
                () -> Assertions.assertEquals(mockPost.getContent(), result.getContent()),
                () -> Assertions.assertEquals(mockPost.getCreationTime(), result.getCreationTime()),
                () -> Assertions.assertEquals(mockPost.getUser(), result.getUser()),
                () -> Assertions.assertEquals(mockPost.getTags(), result.getTags())
        );
    }

    @Test
    public void create_should_throw_when_userIsBlocked() {
        // Arrange
        Post mockPost = createMockPost();
        User mockUser = createBlockedUser();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.create(mockUser, mockPost));
    }

    @Test
    public void create_should_callRepository_when_userIsActive() {
        // Arrange
        Post mockPost = createMockPost();
        User mockUser = createMockUser();

        // Act
        service.create(mockUser, mockPost);

        // Assert
        Mockito.verify(mockPostRepository, Mockito.times(1))
                .create(mockPost);
    }

    @Test
    public void addTags_should_throw_when_userIsBlockedOwner() {
        // Arrange
        Post mockPost = createMockPost();
        User mockUser = createBlockedUser();
        List<String> tags = createMockTagList();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () ->
                service.addTags(mockUser, mockPost.getPostId(), tags));
    }


    @Test
    public void addTags_should_throw_when_userIsBlockedAdmin() {
        // Arrange
        Post mockPost = createMockPost();
        User mockUser = createBlockedAdmin();
        List<String> tags = createMockTagList();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () ->
                service.addTags(mockUser, mockPost.getPostId(), tags));
    }

    @Test
    public void createTags_should_callPostRepository_when_userIsActiveOwner() {
        // Arrange
        Post mockPost = createMockPost();
        User mockUser = createMockUser();
        List<String> tags = createMockTagList();

        Mockito.when(mockPostRepository.getById(Mockito.anyInt()))
                .thenReturn(mockPost);
        //Act
        service.addTags(mockUser, mockPost.getPostId(), tags);

        // Act, Assert
        Mockito.verify(mockPostRepository, Mockito.times(1))
                .update(mockPost);
    }

    @Test
    public void createTags_should_callTagRepository_when_userIsActiveOwner() {
        // Arrange
        Post mockPost = createMockPost();
        User mockUser = createMockUser();
        Tag mockTag = createMockTag();
        List<String> tags = createMockTagList();

        Mockito.when(mockPostRepository.getById(Mockito.anyInt()))
                .thenReturn(mockPost);
        //Act
        service.addTags(mockUser, mockPost.getPostId(), tags);

        // Act, Assert
        Mockito.verify(mockTagRepository, Mockito.times(1))
                .create(mockTag);
    }

    @Test
    public void createTags_should_callRepository_when_userIsActiveAdmin() {
        // Arrange
        Post mockPost = createMockPost();
        User mockAdmin = createMockAdmin();
        List<String> tags = createMockTagList();

        Mockito.when(mockPostRepository.getById(Mockito.anyInt()))
                .thenReturn(mockPost);
        //Act
        service.addTags(mockAdmin, mockPost.getPostId(), tags);

        // Act, Assert
        Mockito.verify(mockPostRepository, Mockito.times(1))
                .update(mockPost);
    }


    @Test
    public void createTags_should_callTagRepository_when_userIsActiveAdmin() {
        // Arrange
        Post mockPost = createMockPost();
        User mockAdmin = createMockAdmin();
        Tag mockTag = createMockTag();
        List<String> tags = createMockTagList();

        Mockito.when(mockPostRepository.getById(Mockito.anyInt()))
                .thenReturn(mockPost);
        //Act
        service.addTags(mockAdmin, mockPost.getPostId(), tags);

        // Act, Assert
        Mockito.verify(mockTagRepository, Mockito.times(1))
                .create(mockTag);
    }

    @Test
    public void update_should_throw_when_userIsBlockedOwner() {
        // Arrange
        Post mockPost = createMockPost();
        int postId = mockPost.getPostId();
        User mockUser = createBlockedUser();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () ->
                service.update(postId, mockPost, mockUser));
    }

    @Test
    public void update_should_throw_when_userIsNotOwner() {
        // Arrange
        Post mockPost = createMockPost();
        int postId = mockPost.getPostId();
        User mockUser = createNotPostOwner();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () ->
                service.update(postId, mockPost, mockUser));
    }

    @Test
    public void update_should_callTagRepository_when_userIsActiveOwner() {
        // Arrange
        User mockUser = createMockUser();
        Post mockPost = createMockPost();
        int postId = mockPost.getPostId();

        //Act
        service.update(postId, mockPost, mockUser);

        // Act, Assert
        Mockito.verify(mockPostRepository, Mockito.times(1))
                .update(mockPost);
    }


    @Test
    public void userTagUpdate_should_throw_when_userIsBlockedOwner() {
        // Arrange
        Post mockPost = createMockPost();
        int postId = mockPost.getPostId();
        int tagId = createMockTag().getTagId();
        String mockTagName = "mockUpdateTag";
        User mockUser = createBlockedUser();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () ->
                service.userTagUpdate(mockTagName, postId, tagId, mockUser));
    }

    @Test
    public void adminTagUpdate_should_throw_when_userIsBlockedAdmin() {
        // Arrange
        int tagId = createMockTag().getTagId();
        String mockTagName = "mockUpdateTag";
        User mockAdmin = createBlockedAdmin();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () ->
                service.adminTagUpdate(mockTagName, tagId, mockAdmin));
    }


    @Test
    public void userTagUpdate_should_callTagRepository_when_userIsActiveOwner() {
        // Arrange
        Post mockPost = createMockPost();
        User mockUser = createMockUser();
        int postId = mockPost.getPostId();
        Tag mockTag = createMockTag();
        int tagId = mockTag.getTagId();
        String mockTagName = "mockUpdateTag";

        Mockito.when(mockPostRepository.getById(Mockito.anyInt()))
                .thenReturn(mockPost);

        Mockito.when(mockTagRepository.getByName(Mockito.anyString()))
                .thenReturn(Optional.of(mockTag));

        Mockito.when(mockTagRepository.getByName(Mockito.anyString()))
                .thenReturn(Optional.of(mockTag));
        //Act
        service.userTagUpdate(mockTagName, postId, tagId, mockUser);

        // Act, Assert
        Mockito.verify(mockPostRepository, Mockito.times(1))
                .update(mockPost);
    }

    @Test
    public void adminTagUpdate_should_callTagRepository_when_userIsActiveAdmin() {
        // Arrange
        User mockAdmin = createMockAdmin();
        Tag mockTag = createMockTag();
        String mockUpdateTag = "mockUpdateTag";

        Mockito.when(mockTagRepository.getById(Mockito.anyInt())).thenReturn(mockTag);
        //Act
        service.adminTagUpdate(mockUpdateTag, mockTag.getTagId(), mockAdmin);

        // Act, Assert
        Mockito.verify(mockTagRepository, Mockito.times(1))
                .update(mockTag);
    }

    @Test
    public void userTagDelete_should_throw_when_userIsBlockedOwner() {
        // Arrange
        Post mockPost = createMockPost();
        int postId = mockPost.getPostId();
        int tagId = createMockTag().getTagId();
        User mockUser = createBlockedUser();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () ->
                service.userTagDelete(tagId, postId, mockUser));
    }

    @Test
    public void userTagDelete_should_throw_when_userIsNotOwner() {
        // Arrange
        Post mockPost = createMockPost();
        mockPost.setUser(createMockUser());
        int postId = mockPost.getPostId();
        int tagId = createMockTag().getTagId();
        User mockUser = createMockUser();
        mockUser.setUserId(2);

        Mockito.when(mockPostRepository.getById(Mockito.anyInt())).thenReturn(mockPost);

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () ->
                service.userTagDelete(tagId, postId, mockUser));
    }

    @Test
    public void userTagDelete_should_throw_when_tagAndPostAreNotConnected() {
        // Arrange
        Post mockPost = createMockPost();
        int postId = mockPost.getPostId();
        int tagId = createMockTag().getTagId() + 1;
        User mockUser = createMockUser();

        Mockito.when(mockPostRepository.getById(Mockito.anyInt())).thenReturn(mockPost);

        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class, () ->
                service.userTagDelete(tagId, postId, mockUser));
    }

    @Test
    public void userTagDelete_should_callRepository_when_userIsActiveOwner() {
        // Arrange
        Post mockPost = createMockPost();
        int postId = mockPost.getPostId();
        int tagId = createMockTag().getTagId();
        User mockUser = createMockUser();

        Mockito.when(mockPostRepository.getById(Mockito.anyInt())).thenReturn(mockPost);

        //Act
        service.userTagDelete(tagId, postId, mockUser);

        // Act, Assert
        Mockito.verify(mockTagRepository, Mockito.times(1)).userTagDelete(tagId, postId);
    }


    @Test
    public void adminTagDelete_should_throw_when_userIsBlockedAdmin() {
        // Arrange
        int tagId = createMockTag().getTagId();
        User mockAdmin = createBlockedAdmin();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.adminTagDelete(tagId, mockAdmin));
    }

    @Test
    public void adminTagDelete_should_throw_when_userIsNotAdmin() {
        // Arrange
        int tagId = createMockTag().getTagId() + 1;
        User mockUser = createMockUser();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.adminTagDelete(tagId, mockUser));
    }

    @Test
    public void adminTagDelete_should_callRepository_when_userIsActiveAdmin() {
        // Arrange;
        int tagId = createMockTag().getTagId();
        User mockAdmin = createMockAdmin();

        //Act
        service.adminTagDelete(tagId, mockAdmin);

        // Act, Assert
        Mockito.verify(mockTagRepository, Mockito.times(1)).adminTagDelete(tagId);
    }

    @Test
    public void removeTag_should_throw_when_userIsBlockedAdmin() {
        // Arrange
        int tagId = createMockTag().getTagId();
        User mockAdmin = createBlockedAdmin();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () ->
                service.adminTagDelete(tagId, mockAdmin));
    }

    @Test
    public void upVote_should_throw_when_userIsBlockedOwner() {
        // Arrange
        Post mockPost = createMockPost();
        PostVote mockPostVote = createMockPostVote();
        int postId = mockPost.getPostId();
        User mockUser = createBlockedUser();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () ->
                service.upVote(postId, mockUser, mockPostVote));
    }

    @Test
    public void upVote_should_throw_when_userIsBlockedAdmin() {
        // Arrange
        PostVote mockPostVote = createMockPostVote();
        int postId = createMockPost().getPostId();
        User mockAdmin = createBlockedAdmin();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () ->
                service.upVote(postId, mockAdmin, mockPostVote));
    }

    @Test
    public void upVote_should_callVoteRepositoryUpdate_when_userIsActive() {
        // Arrange
        PostVote mockPostVote = createMockPostVote();
        Optional <PostVote> optionalPostVote = Optional.of(mockPostVote);
        int postId = createMockPost().getPostId();
        User mockUser = createMockUser();

        Mockito.when(mockVoteRepository.getByUserAndPost
                (Mockito.anyInt(), Mockito.anyInt())).thenReturn(optionalPostVote);
        //Act
        service.upVote(postId, mockUser, mockPostVote);

        // Act, Assert
        Mockito.verify(mockVoteRepository, Mockito.times(1))
                .update(mockPostVote);
    }

    @Test
    public void upVote_should_callVoteRepositoryCreate_when_userIsActive() {
        // Arrange
        PostVote mockPostVote = createMockPostVote();
        Optional <PostVote> optionalPostVote = Optional.empty();
        int postId = createMockPost().getPostId();
        User mockUser = createMockUser();

        Mockito.when(mockVoteRepository.getByUserAndPost
                        (Mockito.anyInt(), Mockito.anyInt())).thenReturn(optionalPostVote);
        //Act
        service.upVote(postId, mockUser, mockPostVote);

        // Act, Assert
        Mockito.verify(mockVoteRepository, Mockito.times(1))
                .create(mockPostVote);
    }

    @Test
    public void deleteVote_should_throw_when_userIsBlockedOwner() {
        // Arrange
        int postId = createMockPost().getPostId();
        User mockUser = createBlockedUser();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () ->
                service.deleteVote(postId, mockUser));
    }

    @Test
    public void deleteVote_should_throw_when_userIsBlockedAdmin() {
        // Arrange
        int postId = createMockPost().getPostId();
        User mockAdmin = createBlockedAdmin();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () ->
                service.deleteVote(postId, mockAdmin));
    }

    @Test
    public void deleteVote_should_throw_when_voteDoesNotExist() {
        // Arrange
        Optional <PostVote> optionalPostVote = Optional.empty();
        int postId = createMockPost().getPostId();
        User mockUser = createMockUser();

        Mockito.when(mockVoteRepository.getByUserAndPost
                (Mockito.anyInt(), Mockito.anyInt())).thenReturn(optionalPostVote);

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.deleteVote(postId, mockUser));
    }

    @Test
    public void deleteVote_should_callVoteRepository_when_userIsActive() {
        // Arrange
        PostVote mockPostVote = createMockPostVote();
        Optional <PostVote> optionalPostVote = Optional.of(mockPostVote);
        int postId = createMockPost().getPostId();
        User mockUser = createMockUser();

        Mockito.when(mockVoteRepository.getByUserAndPost
                (Mockito.anyInt(), Mockito.anyInt())).thenReturn(optionalPostVote);
        //Act
        service.deleteVote(postId, mockUser);

        //Assert
        Mockito.verify(mockVoteRepository, Mockito.times(1))
                .delete(mockPostVote);
    }

    @Test
    public void delete_should_throw_when_userIsBlockedOwner() {
        // Arrange
        Post mockPost = createMockPost();
        int postId = mockPost.getPostId();
        User mockUser = createBlockedUser();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () ->
                service.delete(postId, mockUser));
    }

    @Test
    public void delete_should_throw_when_userIsBlockedAdmin() {
        // Arrange
        Post mockPost = createMockPost();
        int postId = mockPost.getPostId();
        User mockAdmin = createBlockedAdmin();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () ->
                service.delete(postId, mockAdmin));
    }

    @Test
    public void delete_should_callPostRepository_when_userIsActiveOwner() {
        // Arrange
        Post mockPost = createMockPost();
        int postId = mockPost.getPostId();
        User mockUser = createMockUser();

        Mockito.when(mockPostRepository.getById( Mockito.anyInt())).thenReturn(mockPost);
        //Act

        service.delete(postId, mockUser);

        //Assert
        Mockito.verify(mockPostRepository, Mockito.times(1))
                .delete(mockPost);
    }
}
