create or replace table tags
(
    tag_id   int auto_increment
        primary key,
    tag_name varchar(32) not null,
    constraint tags_tag_uindex
        unique (tag_name)
);

create or replace table types
(
    type_id   int         not null
        primary key,
    user_role varchar(15) null
)
    comment 'user, moderator or admin' charset = latin1;

create or replace table users
(
    user_id        int auto_increment
        primary key,
    username       varchar(32)          not null,
    password       varchar(32)          not null,
    first_name     varchar(32)          not null,
    last_name      varchar(32)          not null,
    email          varchar(32)          not null,
    blocked_status tinyint(1) default 0 null,
    constraint users_e_mail_uindex
        unique (email),
    constraint users_username_uindex
        unique (username)
)
    charset = latin1;

create or replace table images
(
    user_id int      not null
        primary key,
    content longblob null,
    constraint images_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create or replace table phones
(
    user_id int         not null
        primary key,
    phone   varchar(10) not null,
    constraint telephones_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create or replace table posts
(
    post_id       int auto_increment
        primary key,
    title         varchar(64)                          not null,
    content       varchar(8192)                        not null,
    creation_time datetime default current_timestamp() null,
    user_id       int                                  not null,
    constraint posts_users_user_id_fk
        foreign key (user_id) references users (user_id)
)
    charset = latin1;

create or replace table comments
(
    comment_id int auto_increment
        primary key,
    content    varchar(8192) not null,
    user_id    int           not null,
    post_id    int           not null,
    constraint comments_posts_post_id_fk
        foreign key (post_id) references posts (post_id),
    constraint comments_users_user_id_fk
        foreign key (user_id) references users (user_id)
)
    comment 'of posts' charset = latin1;

create or replace table post_tags
(
    tag_id  int not null,
    post_id int not null,
    constraint post_tags_posts_post_id_fk
        foreign key (post_id) references posts (post_id),
    constraint post_tags_tags_tag_id_fk
        foreign key (tag_id) references tags (tag_id)
)
    charset = latin1;

create or replace table users_types
(
    user_id int not null,
    type_id int not null,
    constraint users_types_types_type_id_fk
        foreign key (type_id) references types (type_id),
    constraint users_types_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create or replace table votes
(
    vote_id   int auto_increment
        primary key,
    vote_type varchar(32) not null
);

create or replace table posts_votes
(
    post_vote_id int auto_increment
        primary key,
    vote_id      int not null,
    user_id      int not null,
    post_id      int not null,
    constraint posts_votes_posts_post_id_fk
        foreign key (post_id) references posts (post_id),
    constraint posts_votes_users_user_id_fk
        foreign key (user_id) references users (user_id),
    constraint posts_votes_votes_vote_id_fk
        foreign key (vote_id) references votes (vote_id)
);

